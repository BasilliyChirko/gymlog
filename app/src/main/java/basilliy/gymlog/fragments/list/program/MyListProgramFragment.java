package basilliy.gymlog.fragments.list.program;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import basilliy.gymlog.R;
import basilliy.gymlog.datamodel.Program;
import basilliy.gymlog.interfaces.ChildFragment;
import basilliy.gymlog.main.DataBase;
import basilliy.gymlog.main.NavigationActivity;

public class MyListProgramFragment extends Fragment {

    private ArrayList<Program> data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_program, container, false);

        data = DataBase.with(getContext()).getAllProgram();
        RecyclerView rView = (RecyclerView) view.findViewById(R.id.recycler_list);
        rView.setAdapter(new RecyclerAdapter());
        rView.setLayoutManager(new LinearLayoutManager(getContext()));
        rView.setItemAnimator(new DefaultItemAnimator());
        return view;
    }


    private class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getContext())
                    .inflate(R.layout.list_element_program, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final Program program = data.get(position);
            holder.name.setText(program.getName());

            holder.onClick(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO: 12.02.2017 onClickProgram
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;

        private View view;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.text_name);
            this.view = view;
        }

        private void onClick(View.OnClickListener listener) {
            view.setOnClickListener(listener);
        }
    }
}
