package basilliy.gymlog.fragments.list.program;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import basilliy.gymlog.R;
import basilliy.gymlog.interfaces.ChildFragment;
import basilliy.gymlog.main.DataBase;
import basilliy.gymlog.datamodel.Day;
import basilliy.gymlog.datamodel.Exercise;
import basilliy.gymlog.datamodel.Program;

public class ProgramDetailFragment extends Fragment implements
        ChildFragment {
    private Program program;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_program_detail, container, false);
        RecyclerView rList = (RecyclerView) v.findViewById(R.id.recycler_list);
        program = DataBase.with(getActivity()).getProgram(getArguments().getInt("id"));
        rList.setAdapter(new RecyclerAdapter());
        rList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rList.setItemAnimator(new DefaultItemAnimator());

        ((TextView) v.findViewById(R.id.text_name)).setText(program.getName());

        View lLevel = v.findViewById(R.id.layout_level);
        View lMale = v.findViewById(R.id.layout_male);
        View lBodyType = v.findViewById(R.id.layout_body_type);
        View lTarget = v.findViewById(R.id.layout_target);
        TextView description = (TextView) v.findViewById(R.id.text_description);
        TextView days = (TextView) v.findViewById(R.id.text_days);

        days.setText(String.valueOf(program.getDays()));

        if (program.getDescription() != null) {
            description.setVisibility(View.VISIBLE);
            description.setText(program.getDescription());
        } else description.setVisibility(View.GONE);

        if (program.getLevel() != -1) {
            lLevel.setVisibility(View.VISIBLE);
            ((TextView)lLevel.findViewById(R.id.text_level)).setText(program.getLevelString());
        } else lLevel.setVisibility(View.GONE);

        if (program.getMale() != -1) {
            lMale.setVisibility(View.VISIBLE);
            ((TextView) lMale.findViewById(R.id.text_male)).setText(program.getMaleString());
        } else lMale.setVisibility(View.GONE);

        if (program.getBodyType() != null) {
            lBodyType.setVisibility(View.VISIBLE);
            ((TextView) lBodyType.findViewById(R.id.text_body_type)).setText(program.getBodyType());
        } else lBodyType.setVisibility(View.GONE);

        if (program.getTarget() != null) {
            lTarget.setVisibility(View.VISIBLE);
            ((TextView) lTarget.findViewById(R.id.text_target)).setText(program.getTarget());
        } else lTarget.setVisibility(View.GONE);
        return v;
    }

    public static ProgramDetailFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt("id", id);
        ProgramDetailFragment fragment = new ProgramDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Fragment setParentFragment(Fragment fragment) {
        return this;
    }


    private class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.list_element_program_day, parent, false
            ));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Day day = program.getDay(position);
            if (day != null) {
                Resources resources = getContext().getResources();
                String sDay = resources.getString(R.string.day) +
                        ": " + (day.getNumber() + 1) + " " + day.getName();
                holder.dayName.setText(sDay);


                holder.layoutExercise.removeAllViews();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                for (Exercise e : day.getExerciseList()) {
                    View view = inflater.inflate(R.layout.item_part_exercise, holder.layoutExercise, false);
                    TextView name = (TextView) view.findViewById(R.id.text_exercise_name);
                    TextView aproach = (TextView) view.findViewById(R.id.text_approach_count);
                    TextView reps = (TextView) view.findViewById(R.id.text_reps_count);

                    name.setText(e.getName());
                    String s = resources.getString(R.string.label_approaches) + " " + e.getApproachList().size();
                    aproach.setText(s);
                    s = resources.getString(R.string.label_reps) + " " + e.getRepsString();
                    reps.setText(s);

                    holder.layoutExercise.addView(view);
                }


                holder.onClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        notifyItemChanged(holder.getAdapterPosition(), 1);
                        holder.fullMode = !holder.fullMode;
                        if (holder.fullMode) holder.layoutExercise.setVisibility(View.VISIBLE);
                        else holder.layoutExercise.setVisibility(View.GONE);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return program.getDayList().size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView dayName;
        private LinearLayout layoutExercise;
        private boolean fullMode = false;
        private View view;

        ViewHolder(View view) {
            super(view);
            dayName = (TextView) view.findViewById(R.id.text_day_name);
            layoutExercise = (LinearLayout) view.findViewById(R.id.layout_exercise);
            this.view = view;
        }

        void onClick(View.OnClickListener listener) {
            view.setOnClickListener(listener);
        }
    }
}
