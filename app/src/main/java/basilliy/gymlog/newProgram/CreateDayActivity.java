package basilliy.gymlog.newProgram;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import basilliy.gymlog.R;
import basilliy.gymlog.datamodel.Day;
import basilliy.gymlog.datamodel.Exercise;
import basilliy.gymlog.main.D;
import basilliy.gymlog.main.DataBase;
import basilliy.gymlog.pattern.TextChangedListener;
import basilliy.gymlog.pattern.ToolbarActivity;

public class CreateDayActivity extends ToolbarActivity {

    private Day day;
    private RecyclerAdapter adapter;
    private TextView tCount;
    private int marker = -1;

    private View layoutMarker;
    private View layoutNormal;

    private static final int REQUEST = 1001;

    public static void start(Context context, int dayNumber) {
        Intent starter = new Intent(context, CreateDayActivity.class);
        starter.putExtra("day", dayNumber);
        starter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_day);

        Toolbar toolbar = initToolbar(R.id.toolbar);
        addToolbarFirstLine(R.id.layout_toolbar_create_day);
        setDisplayHomeAsUpEnabled(true);

        EditText title = (EditText) toolbar.findViewById(R.id.edit_title);
        TextView labelDayNumber = (TextView) toolbar.findViewById(R.id.label_text);
        layoutMarker = findViewById(R.id.layout_toolbar_marker);
        layoutNormal = findViewById(R.id.layout_toolbar_normal);
        tCount = (TextView) layoutNormal.findViewById(R.id.count);

        day = D.CREATED_PROGRAM.getDay(getIntent().getExtras().getInt("day"));


        title.addTextChangedListener(new TextChangedListener() {
            @Override
            public void afterTextChanged(Editable s) {
                day.setName(s.toString());
            }
        });

        layoutNormal.findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getApplicationContext(), SelectMuscleActivity.class), REQUEST);
            }
        });

        layoutMarker.findViewById(R.id.button_up).setOnClickListener(clickUp);
        layoutMarker.findViewById(R.id.button_down).setOnClickListener(clickDown);
        layoutMarker.findViewById(R.id.button_delete).setOnClickListener(clickDelete);

        labelDayNumber.setText(getResources().getString(R.string.day) + " " + (day.getNumber() + 1));


        if (day.getName() != null)
            title.setText(day.getName());

        adapter = new RecyclerAdapter();
        RecyclerView rList = (RecyclerView) findViewById(R.id.recycler_list);
        rList.setItemAnimator(new DefaultItemAnimator());
        rList.setLayoutManager(new LinearLayoutManager(this));
        rList.setAdapter(adapter);

        notifyExerciseCount();
        setToolbarMarker();

    }

    private void setToolbarMarker() {
        layoutMarker.setVisibility(marker != -1 ? View.VISIBLE : View.GONE);
        layoutNormal.setVisibility(marker != -1 ? View.GONE : View.VISIBLE);
    }


    private void notifyExerciseCount() {
        tCount.setText(String.valueOf(day.getExerciseList().size()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if (marker != -1) {
            int pos = marker;
            marker = -1;
            adapter.notifyItemChanged(pos);
            setToolbarMarker();
        } else super.onBackPressed();
    }

    private class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_element_create_day_exercise, parent, false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final Exercise exercise = day.getExercise(position);
            holder.name.setText(exercise.getName());

            if (marker == holder.getAdapterPosition())
                holder.layout.setBackgroundResource(R.color.colorAccent);
            else holder.layout.setBackgroundResource(R.color.main_white);

            boolean b = exercise.getApproachSize() > 0;

            holder.approach.setVisibility(b ? View.VISIBLE : View.GONE);
            holder.reps.setVisibility(b ? View.VISIBLE : View.GONE);
            holder.weight.setVisibility(b ? View.VISIBLE : View.GONE);

            holder.approach.setText(R.string.approach);
            holder.approach.append(" " + exercise.getApproachSize());

            holder.reps.setText(R.string.reps);
            holder.reps.append(" " + exercise.getRepsString());

            holder.weight.setText(R.string.weight);
            holder.weight.append(" " + exercise.getWeightString());

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (marker != -1)
                        adapter.notifyItemChanged(marker);
                    if (marker == holder.getAdapterPosition())
                        marker = -1;
                    else
                        marker = holder.getAdapterPosition();
                    adapter.notifyItemChanged(marker);
                    setToolbarMarker();
                }
            });

            holder.next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CreateExerciseActivity.start(
                            getApplicationContext(),
                            day.getNumber(),
                            holder.getAdapterPosition()
                    );
                }
            });
        }

        @Override
        public int getItemCount() {
            return day.getExerciseList().size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private View view;
        private View layout;
        private View next;
        private TextView approach;
        private TextView reps;
        private TextView weight;

        ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.text_name);
            layout = view.findViewById(R.id.layout);
            next = view.findViewById(R.id.button_next);
            approach = (TextView) view.findViewById(R.id.approach);
            reps = (TextView) view.findViewById(R.id.reps);
            weight = (TextView) view.findViewById(R.id.weight);
            this.view = view;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST && resultCode == RESULT_OK) {
            day.addExercise(DataBase.with(this).getExerciseStore(data.getIntExtra("result", 0)));
            adapter.notifyDataSetChanged();
            notifyExerciseCount();
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    View.OnClickListener clickUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (marker > 0) {
                day.replaceExercise(marker, marker - 1);
                marker--;
                adapter.notifyItemMoved(marker + 1, marker);

                adapter.notifyItemChanged(marker);
                adapter.notifyItemChanged(marker + 1);
            }
        }
    };

    View.OnClickListener clickDown = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (marker < day.getExerciseSize() - 1) {
                day.replaceExercise(marker, marker + 1);
                marker++;
                adapter.notifyItemMoved(marker - 1, marker);

                adapter.notifyItemChanged(marker);
                adapter.notifyItemChanged(marker - 1);
            }
        }
    };

    View.OnClickListener clickDelete = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            day.removeExercise(marker);
            notifyExerciseCount();
            int pos = marker;
            marker = -1;
            adapter.notifyItemRemoved(pos);
            adapter.notifyItemRangeChanged(pos, day.getExerciseSize() - pos);
            setToolbarMarker();
        }
    };

}
