package basilliy.gymlog.newProgram;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import basilliy.gymlog.R;
import basilliy.gymlog.datamodel.Approach;
import basilliy.gymlog.datamodel.Exercise;
import basilliy.gymlog.main.D;

import static android.content.Context.MODE_PRIVATE;
import static basilliy.gymlog.main.D.conts.APPROACH;
import static basilliy.gymlog.main.D.conts.DAY;
import static basilliy.gymlog.main.D.conts.EXERCISE;


public class SetApproachDataDialog extends DialogFragment {

    private int reps;
    private int weight;

    private int day;
    private int exercise;
    private int approach;

    private Listener listener;

    public static final int DELETE = 1;
    public static final int CREATE = 2;
    public static final int CHANGE = 3;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_set_approach_data, null, false);

        final EditText etReps = (EditText) v.findViewById(R.id.reps);
        final EditText etWeight = (EditText) v.findViewById(R.id.weight);
        Button bDelete = (Button) v.findViewById(R.id.button_delete);
        Button bDone = (Button) v.findViewById(R.id.button_done);

        Bundle arguments = getArguments();

        SharedPreferences p = getActivity().getSharedPreferences(D.new_program.preferences.file, MODE_PRIVATE);
        reps = p.getInt(D.new_program.preferences.last_reps, 15);
        weight = p.getInt(D.new_program.preferences.last_weight, 30);


        day = arguments.getInt(DAY);
        exercise = arguments.getInt(EXERCISE);
        approach = arguments.getInt(APPROACH, -1);
        final boolean b = approach != -1;

        etReps.setText(String.valueOf(reps));
        etWeight.setText(String.valueOf(weight));


        bDelete.setVisibility(b ? View.VISIBLE : View.GONE);
        if (b) bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                D.CREATED_PROGRAM.getDay(day).getExercise(exercise).removeApproach(approach);
                listener.onApproachDataChanged(DELETE, approach);
                dismiss();
            }
        });

        bDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reps = Integer.parseInt(etReps.getText().toString());
                weight = Integer.parseInt(etWeight.getText().toString());

                if (b) {
                    Approach a = D.CREATED_PROGRAM.getDay(day).getExercise(exercise).getApproach(approach);
                    a.setReps(reps);
                    a.setWeight(weight);
                    listener.onApproachDataChanged(CHANGE, approach);
                } else {
                    Approach a = new Approach();
                    a.setReps(reps);
                    a.setWeight(weight);
                    Exercise e = D.CREATED_PROGRAM.getDay(day).getExercise(exercise);
                    e.addApproach(a);
                    listener.onApproachDataChanged(CREATE, e.getApproachSize() - 1);
                }
                dismiss();
            }
        });


        return new AlertDialog.Builder(getContext()).setView(v).create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (Listener) context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getSharedPreferences(D.new_program.preferences.file, MODE_PRIVATE)
                .edit()
                .putInt(D.new_program.preferences.last_reps, reps)
                .putInt(D.new_program.preferences.last_weight, weight)
                .apply();
    }

    public static SetApproachDataDialog newInstance(int day, int exercise) {
        Bundle args = new Bundle();
        args.putInt(DAY, day);
        args.putInt(EXERCISE, exercise);
        SetApproachDataDialog fragment = new SetApproachDataDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static SetApproachDataDialog newInstance(int day, int exercise, int approach) {
        Bundle args = new Bundle();
        args.putInt(DAY, day);
        args.putInt(EXERCISE, exercise);
        args.putInt(APPROACH, approach);
        SetApproachDataDialog fragment = new SetApproachDataDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public interface Listener {
        void onApproachDataChanged(int state, int approach);
    }
}
