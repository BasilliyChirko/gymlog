package basilliy.gymlog.newProgram;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import basilliy.gymlog.R;
import basilliy.gymlog.datamodel.Exercise;
import basilliy.gymlog.main.DataBase;
import basilliy.gymlog.pattern.ToolbarActivity;

public class SelectExerciseDetailActivity extends ToolbarActivity {

    public static final String EXERCISE = "exercise";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_muscle_detail);
        setResult(RESULT_CANCELED);
        initToolbar(R.id.toolbar);
        setDisplayHomeAsUpEnabled(true);

        int id = getIntent().getIntExtra(EXERCISE, 0);
        final Exercise exercise = DataBase.with(this).getExerciseStore(id);

        TextView title = (TextView) findViewById(R.id.label);
        TextView targetMuscle = (TextView) findViewById(R.id.target_muscle);
        TextView extraMuscle = (TextView) findViewById(R.id.extra_muscle);
        TextView level = (TextView) findViewById(R.id.level);
        TextView inventory = (TextView) findViewById(R.id.inventory);
        TextView description = (TextView) findViewById(R.id.description);
        TextView instruction = (TextView) findViewById(R.id.instruction);
        TextView advice = (TextView) findViewById(R.id.advice);
        View button = findViewById(R.id.button_add);
        View done = findViewById(R.id.button_done);

        title.setText(exercise.getName());

        level.append(" " + exercise.getLevelString());
        targetMuscle.append(" " + exercise.getTargetMuscle());
        description.append("\n" + exercise.getDescription());
        instruction.append("\n" + exercise.getInstruction());
        advice.append("\n" + exercise.getAdvice());

        for (String s : exercise.getInventory().split(", "))
            inventory.append("\n" + s);

        for (String s : exercise.getInvolvesMuscleArray()) {
            extraMuscle.append("\n" + s);
            if (s.equals("-")) {
                extraMuscle.setVisibility(View.GONE);
                break;
            }
        }

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent result = new Intent();
                result.putExtra("result", exercise.getStoreId());
                setResult(RESULT_OK, result);
                finish();
            }
        };

        button.setOnClickListener(listener);
        done.setOnClickListener(listener);
    }
}
