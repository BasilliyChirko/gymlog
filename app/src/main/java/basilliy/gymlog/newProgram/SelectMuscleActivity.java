package basilliy.gymlog.newProgram;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import basilliy.gymlog.R;
import basilliy.gymlog.pattern.ToolbarActivity;

public class SelectMuscleActivity extends ToolbarActivity {

    private String[] aMuscle = new String[] {
            "Бицепс",
            "Шея",
            "Плечи",
            "Трапеции",
            "Предплечья",
            "Грудь",
            "Пресс",
            "Трицепс",
            "Бедра",
            "Широчайшие мышцы спины",
            "Ягодицы",
            "Икры",
            "Квадрицепс",
            "Вторая часть спины",
            "Нижняя часть спины"
    };

    public static final int REQUEST = 1002;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_muscle);
        initToolbar(R.id.toolbar);
        setDisplayHomeAsUpEnabled(true);
        setResult(RESULT_CANCELED);


        RecyclerAdapter adapter = new RecyclerAdapter();
        RecyclerView rList = (RecyclerView) findViewById(R.id.recycler_list);
        rList.setItemAnimator(new DefaultItemAnimator());
        rList.setLayoutManager(new LinearLayoutManager(this));
        rList.setAdapter(adapter);
    }




    private class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_element_muscle, parent, false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final String name = aMuscle[holder.getAdapterPosition()];
            holder.name.setText(name);

            View.OnClickListener click = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), SelectExerciseActivity.class);
                    intent.putExtra(SelectExerciseActivity.MUSCLE, name);
                    startActivityForResult(intent, REQUEST);
                }
            };

            holder.view.setOnClickListener(click);
            holder.add.setOnClickListener(click);
        }

        @Override
        public int getItemCount() {
            return aMuscle.length;
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private View view;
        private View add;

        ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            add = view.findViewById(R.id.button_add);
            this.view = view;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK, data);
            finish();
        } else super.onActivityResult(requestCode, resultCode, data);
    }
}
