package basilliy.gymlog.newProgram;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import basilliy.gymlog.R;
import basilliy.gymlog.datamodel.Approach;
import basilliy.gymlog.datamodel.Day;
import basilliy.gymlog.datamodel.Exercise;
import basilliy.gymlog.datamodel.Program;
import basilliy.gymlog.main.D;
import basilliy.gymlog.main.DataBase;
import basilliy.gymlog.main.NavigationActivity;
import basilliy.gymlog.pattern.BackPressedFragment;
import basilliy.gymlog.pattern.TextChangedListener;

public class CreateProgramFragment extends BackPressedFragment {

    private Program program;
    private TextView textDays;
    private RecyclerAdapter adapter;
    private int marker = -1;
    private NavigationActivity activity;
    private View view;
    private View markerToolbar;
    private EditText title;

    private void initEmptyProgram() {
        D.CREATED_PROGRAM = new Program();
        D.CREATED_PROGRAM.addDay(new Day());
        D.CREATED_PROGRAM.addDay(new Day());
        D.CREATED_PROGRAM.addDay(new Day());
        D.CREATED_PROGRAM.addDay(new Day());
        D.CREATED_PROGRAM.addDay(new Day());
        program = D.CREATED_PROGRAM;
    }

    private void initTestProgram() {
        D.CREATED_PROGRAM = new Program();
        D.CREATED_PROGRAM.setName("Неделька");

        DataBase base = DataBase.with(getContext());
        D.CREATED_PROGRAM.addDay(new Day().setName("Руки")
                .addExercise(base.getExerciseStore(1)
                        .addApproach(new Approach().setReps(15).setWeight(30))
                )
                .addExercise(base.getExerciseStore(2)
                        .addApproach(new Approach().setReps(15).setWeight(30))
                )
                .addExercise(base.getExerciseStore(3)
                        .addApproach(new Approach().setReps(15).setWeight(30))
                        .addApproach(new Approach().setReps(15).setWeight(30))
                ));
        D.CREATED_PROGRAM.addDay(new Day());
        D.CREATED_PROGRAM.addDay(new Day().setName("Ноги")
                .addExercise(base.getExerciseStore(4)
                        .addApproach(new Approach().setReps(15).setWeight(30))
                )
                .addExercise(base.getExerciseStore(5)
                        .addApproach(new Approach().setReps(15).setWeight(30))
                )
                .addExercise(base.getExerciseStore(6)
                        .addApproach(new Approach().setReps(15).setWeight(30))
                ));
        D.CREATED_PROGRAM.addDay(new Day());
        D.CREATED_PROGRAM.addDay(new Day().setName("Спина")
                .addExercise(base.getExerciseStore(7)
                        .addApproach(new Approach().setReps(15).setWeight(30))
                )
                .addExercise(base.getExerciseStore(8)
                        .addApproach(new Approach().setReps(15).setWeight(30))
                )
                .addExercise(base.getExerciseStore(9)
                        .addApproach(new Approach().setReps(15).setWeight(30))
                ));
        D.CREATED_PROGRAM.addDay(new Day());
        D.CREATED_PROGRAM.addDay(new Day());
        D.CREATED_PROGRAM.recountNumberDays();
        program = D.CREATED_PROGRAM;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        initTestProgram();
        initEmptyProgram();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_nav_new_program, container, false);

        activity = (NavigationActivity) getActivity();
        View toolbar = activity.showToolbarFirstLine(R.id.layout_toolbar_new_program);

        View secondLine = activity.showToolbarSecondLine(R.id.layout_toolbar_new_program_count);
        textDays = (TextView) secondLine.findViewById(R.id.label_length);


        title = (EditText) toolbar.findViewById(R.id.edit_title);
        RecyclerView rList = (RecyclerView) view.findViewById(R.id.recycler_list);

        toolbar.findViewById(R.id.button_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {done();}
        });

        secondLine.findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                program.addDay(new Day().setNumber(program.getDays()));
                setDays();
                adapter.notifyItemInserted(program.getDays() - 1);
            }
        });

        title.addTextChangedListener(new TextChangedListener() {
            @Override
            public void afterTextChanged(Editable s) {
                program.setName(s.toString());
            }
        });

        title.setText(program.getName());
        adapter = new RecyclerAdapter();
        rList.setAdapter(adapter);
        rList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rList.setItemAnimator(new DefaultItemAnimator());

        setDays();


        return view;
    }

    private void setDays() {
        int days = program.getDays();
        String s = days + " ";
        if (days == 0) s += getContext().getResources().getString(R.string.days);
        else if (days == 1) s += getContext().getResources().getString(R.string.day);
        else if (days < 5) s += getContext().getResources().getString(R.string.day_234);
        else s += getContext().getResources().getString(R.string.days);
        textDays.setText(R.string.label_time);
        textDays.append(" " + s);
        program.recountNumberDays();
    }

    private class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.list_element_create_program_day, parent, false
            ));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final Day day = program.getDay(position);
            if (day != null) {
                Resources resources = getContext().getResources();

                if (marker == holder.getAdapterPosition())
                    holder.layout.setBackgroundResource(R.color.colorAccent);
                else holder.layout.setBackgroundResource(R.color.main_white);

                holder.count.setVisibility(day.getExerciseSize() > 0 ? View.VISIBLE : View.GONE);
                holder.count.setText(R.string.label_exercise_count);
                holder.count.append(" " + day.getExerciseSize());

                String sDay;
                if (day.getName() == null) sDay = "Отдых";
                else  sDay = resources.getString(R.string.day) + " " +
                            (day.getNumber() + 1) + " " + day.getName();
                holder.dayName.setText(sDay);

                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (marker != -1)
                            adapter.notifyItemChanged(marker);
                        if (marker == holder.getAdapterPosition())
                            marker = -1;
                        else
                            marker = holder.getAdapterPosition();
                        adapter.notifyItemChanged(marker);
                        initToolbar();
                    }
                });

                holder.detail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CreateDayActivity.start(getContext(), day.getNumber());
                    }
                });

            }
        }

        @Override
        public int getItemCount() {
            return program.getDayList().size();
        }
    }

    private void initToolbar() {
        if (marker == -1)
            activity.showToolbarSecondLine(R.id.layout_toolbar_new_program_count);
        else {
            if (markerToolbar == null) {
                markerToolbar = activity.showToolbarSecondLine(R.id.layout_toolbar_new_program_marker);
                View buttonUp = markerToolbar.findViewById(R.id.button_up);
                View buttonDown = markerToolbar.findViewById(R.id.button_down);
                View buttonDelete = markerToolbar.findViewById(R.id.button_delete);

                buttonUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (marker > 0) {
                            program.replaceDays(marker, marker - 1);
                            marker--;
                            adapter.notifyItemMoved(marker + 1, marker);

                            adapter.notifyItemChanged(marker);
                            adapter.notifyItemChanged(marker + 1);
                        }
                    }
                });

                buttonDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (marker < program.getDayList().size() - 1) {
                            program.replaceDays(marker, marker + 1);
                            marker++;
                            adapter.notifyItemMoved(marker - 1, marker);

                            adapter.notifyItemChanged(marker);
                            adapter.notifyItemChanged(marker - 1);
                        }
                    }
                });

                buttonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        program.removeDay(marker);
                        setDays();
                        int pos = marker;
                        marker = -1;
                        adapter.notifyItemRemoved(pos);
                        adapter.notifyItemRangeChanged(pos, program.getDays() - pos);
                        initToolbar();
                    }
                });
            } else {
                activity.showToolbarSecondLine(R.id.layout_toolbar_new_program_marker);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (marker != -1) {
            int pos = marker;
            marker = -1;
            adapter.notifyItemChanged(pos);
            initToolbar();
            return true;
        }
        return false;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView dayName;
        private View detail;
        private View layout;
        private View view;
        private TextView count;

        ViewHolder(View view) {
            super(view);
            dayName = (TextView) view.findViewById(R.id.text_name);
            detail = view.findViewById(R.id.button_detail);
            layout = view.findViewById(R.id.layout);
            count = (TextView) view.findViewById(R.id.count);
            this.view = view;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    public void done() {
        boolean haveExercise = false;
        boolean b = true;
        m:
        for (Day day : program.getDayList()) {
            for (Exercise exercise : day.getExerciseList()) {
                haveExercise = true;
                if (exercise.getApproachList().isEmpty()) {

                    String text = getString(R.string.day);
                    text += " " + (day.getNumber() + 1);
                    if (day.getName() != null)
                        text += " " + day.getName();
                    text += ", " + exercise.getName();

                    if (text.length() > 45)
                        text = text.substring(0, 42) + "...";

                    text += "\n" + getString(R.string.error_approach);
                    D.snack(view, text);
                    b = false;
                    break m;
                }
            }
        }

        if (!b) return;

        if (!haveExercise) {
            D.snack(view, "Что же это за тренировка если только отдыхать?");
            return;
        }

        if (title.getText().toString().isEmpty()) {
            D.snack(view, "Придумай название своей тринеровке");
            return;
        }

        DataBase.with(getContext()).addProgram(D.CREATED_PROGRAM);
        title.setText("");
        initEmptyProgram();
        adapter.notifyDataSetChanged();
        D.snack(view, "Программа тренировки сохранена");
    }

}
