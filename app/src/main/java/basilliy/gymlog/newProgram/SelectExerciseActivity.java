package basilliy.gymlog.newProgram;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import basilliy.gymlog.R;
import basilliy.gymlog.datamodel.Exercise;
import basilliy.gymlog.main.DataBase;
import basilliy.gymlog.pattern.ToolbarActivity;

public class SelectExerciseActivity extends ToolbarActivity {

    private ArrayList<Exercise> aExercise;

    public static final String MUSCLE = "targetMuscle";
    public static final int REQUEST = 1003;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_exercise);
        initToolbar(R.id.toolbar);
        setDisplayHomeAsUpEnabled(true);
        setResult(RESULT_CANCELED);

        String muscle = getIntent().getExtras().getString(MUSCLE);
        aExercise = DataBase.with(this).getExerciseStore(muscle);

        RecyclerAdapter adapter = new RecyclerAdapter();
        RecyclerView rList = (RecyclerView) findViewById(R.id.recycler_list);
        rList.setItemAnimator(new DefaultItemAnimator());
        rList.setLayoutManager(new LinearLayoutManager(this));
        rList.setAdapter(adapter);
    }


    private class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_element_exercise, parent, false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final Exercise exercise = aExercise.get(position);

            holder.name.setText(exercise.getName());

            holder.level.setText(R.string.label_level);
            holder.level.append(" ");
            holder.level.append(exercise.getLevelString());

            holder.targetMuscle.setText(R.string.target_muscle);
            holder.targetMuscle.append(" ");
            holder.targetMuscle.append(exercise.getTargetMuscle());

            holder.inventory.setText(R.string.inventory);
            for (String s : exercise.getInventory().split(", ")) {
                holder.inventory.append("\n" + s);
            }

            holder.extraMuscle.setText(R.string.extra_muscle);
            for (String s : exercise.getInvolvesMuscleArray()) {
                holder.extraMuscle.append("\n" + s);

                if (s.equals("-")) {
                    holder.extraMuscle.setVisibility(View.GONE);
                    break;
                }
            }

            holder.onClick(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(holder.getAdapterPosition(), 3);
                    holder.fullMode = !holder.fullMode;

                    holder.layoutDetail.setVisibility(holder.fullMode ? View.VISIBLE : View.GONE);
                    holder.button.setVisibility(holder.fullMode ? View.VISIBLE : View.GONE);
                }
            });

            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), SelectExerciseDetailActivity.class);
                    intent.putExtra(SelectExerciseDetailActivity.EXERCISE, exercise.getStoreId());
                    startActivityForResult(intent, REQUEST);
                }
            });

            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent result = new Intent();
                    result.putExtra("result", exercise.getStoreId());
                    setResult(RESULT_OK, result);
                    finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return aExercise.size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView targetMuscle;
        private TextView extraMuscle;
        private TextView level;
        private LinearLayout layoutDetail;
        private TextView inventory;
        private TextView button;
        private View add;
        private View view;

        private boolean fullMode = false;

        ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            targetMuscle = (TextView) view.findViewById(R.id.target_muscle);
            extraMuscle = (TextView) view.findViewById(R.id.extra_muscle);
            level = (TextView) view.findViewById(R.id.level);
            layoutDetail = (LinearLayout) view.findViewById(R.id.layout_detail);
            inventory = (TextView) view.findViewById(R.id.inventory);
            button = (TextView) view.findViewById(R.id.button_detail);
            add = view.findViewById(R.id.button_add);
            this.view = view;
        }

        void onClick(View.OnClickListener listener) {
            layoutDetail.setOnClickListener(listener);
            level.setOnClickListener(listener);
            name.setOnClickListener(listener);
            view.setOnClickListener(listener);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK, data);
            finish();
        } else super.onActivityResult(requestCode, resultCode, data);
    }
}
