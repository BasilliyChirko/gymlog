package basilliy.gymlog.newProgram;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import basilliy.gymlog.R;
import basilliy.gymlog.datamodel.Approach;
import basilliy.gymlog.datamodel.Exercise;
import basilliy.gymlog.main.D;
import basilliy.gymlog.pattern.ToolbarActivity;

public class CreateExerciseActivity extends ToolbarActivity implements SetApproachDataDialog.Listener {

    private Exercise exercise;
    private TextView approachCount;
    private RecyclerAdapter adapter;
    int dID;
    int eID;

    public static void start(Context context, int day, int exercise) {
        Intent starter = new Intent(context, CreateExerciseActivity.class);
        starter.putExtra(D.conts.DAY, day);
        starter.putExtra(D.conts.EXERCISE, exercise);
        starter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_exercise);
        Toolbar toolbar = initToolbar(R.id.toolbar);
        setDisplayHomeAsUpEnabled(true);

        dID = getIntent().getExtras().getInt(D.conts.DAY);
        eID = getIntent().getExtras().getInt(D.conts.EXERCISE);
        exercise = D.CREATED_PROGRAM.getDay(dID).getExercise(eID);

        TextView title = (TextView) toolbar.findViewById(R.id.label);
        approachCount = (TextView) findViewById(R.id.approach);

        title.setText(exercise.getName());
        setApproachCount();

        findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetApproachDataDialog
                        .newInstance(dID, eID)
                        .show(getSupportFragmentManager(), "dialog");
            }
        });

        adapter = new RecyclerAdapter();
        RecyclerView list = (RecyclerView) findViewById(R.id.recycler_list);
        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setItemAnimator(new DefaultItemAnimator());
    }

    public void setApproachCount(){
        approachCount.setText(R.string.approach);
        approachCount.append(" " + exercise.getApproachSize());
    }

    @Override
    public void onApproachDataChanged(int state, int approach) {
        switch (state) {
            case SetApproachDataDialog.DELETE:
                adapter.notifyItemRemoved(approach);
                break;
            case SetApproachDataDialog.CHANGE:
                adapter.notifyItemChanged(approach);
                break;
            case SetApproachDataDialog.CREATE:
                adapter.notifyItemInserted(approach);
                break;
        }
        setApproachCount();
    }


    private class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(getLayoutInflater().inflate(R.layout.list_element_create_exercise_approach, parent, false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Approach approach = exercise.getApproach(position);
            holder.reps.setText(String.valueOf(approach.getReps()));
            holder.weight.setText(String.valueOf(approach.getWeight()));
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SetApproachDataDialog
                            .newInstance(dID, eID, holder.getAdapterPosition())
                            .show(getSupportFragmentManager(), "dialog");
                }
            });
        }

        @Override
        public int getItemCount() {
            return exercise.getApproachSize();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView reps;
        TextView weight;
        View view;

        ViewHolder(View v) {
            super(v);
            reps = (TextView)  v.findViewById(R.id.reps);
            weight = (TextView) v.findViewById(R.id.weight);
            view = v;
        }
    }
}
