package basilliy.gymlog.datamodel;

import java.util.ArrayList;

public class Day {
    private int id;
    private int number;
    private String name;
    private ArrayList<Exercise> exerciseList;

    public int getId() {
        return id;
    }

    public Day setId(int id) {
        this.id = id;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public Day setNumber(int number) {
        this.number = number;
        return this;
    }

    public int getExerciseSize() {
        return getExerciseList().size();
    }

    public int getExerciseIndex(Exercise e) {
        return getExerciseList().indexOf(e);
    }

    public ArrayList<Exercise> getExerciseList() {
        if (exerciseList == null) exerciseList = new ArrayList<>();
        return exerciseList;
    }

    public Day setExerciseList(ArrayList<Exercise> exerciseList) {
        this.exerciseList = exerciseList;
        return this;
    }

    public Exercise getExercise(int index) {
        if (exerciseList == null) return null;
        return exerciseList.get(index);
    }

    public Day addExercise(Exercise Exercise) {
        if (exerciseList == null) exerciseList = new ArrayList<>();
        exerciseList.add(Exercise);
        return this;
    }

    public Day addExercise(int index, Exercise Exercise) {
        if (exerciseList == null) exerciseList = new ArrayList<>();
        exerciseList.add(index, Exercise);
        return this;
    }

    public Day removeExercise(int index) {
        if (exerciseList == null) return this;
        exerciseList.remove(index);
        return this;
    }

    public Day removeExercise(Exercise e) {
        if (exerciseList == null) return this;
        exerciseList.remove(e);
        return this;
    }

    public String getName() {
        return name;
    }

    public Day setName(String name) {
        this.name = name;
        return this;
    }

    public Day replaceExercise(int from, int to) {
        if (from < 0 ||
                to < 0 ||
                from >= getExerciseSize() ||
                to >= getExerciseSize() ||
                from == to ||
                getExerciseSize() == 0)
            return this;

        ArrayList<Exercise> list = getExerciseList();
        Exercise remove = list.remove(from);
        addExercise(to, remove);
        return this;
    }

    public boolean haveId() {
        return id > 0;
    }

    @Override
    public String toString() {
        return "id = " + getId() + ", name = " + getName() + ", number = " + getNumber() + ", e_count = " + getExerciseList().size();
    }
}
