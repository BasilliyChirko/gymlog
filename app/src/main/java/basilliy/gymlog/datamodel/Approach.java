package basilliy.gymlog.datamodel;

public class Approach {
    private int id;
    private int reps; //кол-во раз в подходе
    private float weight; //вес

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReps() {
        return reps;
    }

    public Approach setReps(int reps) {
        this.reps = reps;
        return this;
    }

    public float getAbsWeight() {
        return weight;
    }

    public int getWeight() {
        return (int) weight;
    }

    public Approach setWeight(float weight) {
        this.weight = weight;
        return this;
    }

    public boolean haveId() {
        return id > 0;
    }

}
