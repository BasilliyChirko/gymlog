package basilliy.gymlog.datamodel;

import java.util.ArrayList;

public class Program {
    public static final int MALE = 1;
    public static final int FEMALE = 0;
    public static final int ANY_MALE = 2;

    private int id;
    private int level = -1;
    private String name;
    private String description;
    private String target;
    private String bodyType;
    private int male = -1;
    private ArrayList<Day> dayList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Day> getDayList() {
        if (dayList == null)
            dayList = new ArrayList<>();
        return dayList;
    }

    public void setDayList(ArrayList<Day> dayList) {
        this.dayList = dayList;
    }

    public Day getDay(int index) {
        if (dayList == null) return null;
        return dayList.get(index);
    }


    public void addDay(Day Day) {
        if (dayList == null) dayList = new ArrayList<>();
        dayList.add(Day);
    }

    public void addDay(int index, Day Day) {
        if (dayList == null) dayList = new ArrayList<>();
        dayList.add(index, Day);
    }

    public Day removeDay(int index) {
        if (dayList == null) return null;
        return dayList.remove(index);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDays() {
        return getDayList().size();
    }

    public boolean haveId() {
        return id > 0;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public String getLevelString() {
        switch (level) {
            case 1:
                return "Новичек";
            case 2:
                return "Любитель";
            case 3:
                return "Провессионал";
            default:
                return null;
        }
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public int getMale() {
        return male;
    }

    public String getMaleString() {
        switch (male) {
            case MALE: return "Мужчин";
            case FEMALE: return "Женщин";
            case ANY_MALE: return "Для всех";
        }
        return null;
    }

    public void setMale(int male) {
        this.male = male;
    }

    public void replaceDays(int from, int to) {
        if (from < 0 ||
                to < 0 ||
                from >= getDays() ||
                to >= getDays() ||
                from == to ||
                getDays() == 0)
            return;

        ArrayList<Day> dayList = getDayList();
        Day T1 = dayList.get(from);
        Day T2 = dayList.get(to);

        T1.setNumber(to);
        T2.setNumber(from);

        Day remove = dayList.remove(from);
        addDay(to, remove);

        recountNumberDays();
    }

    public void recountNumberDays() {
        ArrayList<Day> dayList = getDayList();
        for (int i = 0; i < dayList.size(); i++) {
            Day day = dayList.get(i);
            if (day != null)
            day.setNumber(i);
        }
    }
}
