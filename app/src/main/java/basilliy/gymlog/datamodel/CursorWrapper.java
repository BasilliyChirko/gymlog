package basilliy.gymlog.datamodel;

import android.database.Cursor;

public class CursorWrapper {
    private Cursor c;
    public CursorWrapper(Cursor c) {
        this.c = c;
    }

    public boolean moveToFirst() {
        return c.moveToFirst();
    }

    public boolean moveToNext() {
        return c.moveToNext();
    }

    public void close() {
        c.close();
    }

    public int getInt(String column) {
        return c.getInt(c.getColumnIndex(column));
    }

    public String getString(String column) {
        return c.getString(c.getColumnIndex(column));
    }

    public float getFloat(String column) {
        return c.getFloat(c.getColumnIndex(column));
    }
}
