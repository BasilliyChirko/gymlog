package basilliy.gymlog.datamodel;

import java.util.ArrayList;
import java.util.Collections;

public class Exercise {
    private int id;
    private int storeId;
    private int level;
    private String name;
    private String description;
    private String targetMuscle;
    private String inventory;
    private String instruction;
    private String involvedMuscle;
    private String advice;
    private ArrayList<Approach> approachList;

    public int getId() {
        return id;
    }

    public Exercise setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Exercise setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Exercise setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getApproachSize() {
        return getApproachList().size();
    }

    public ArrayList<Approach> getApproachList() {
        if (approachList == null) approachList = new ArrayList<>();
        return approachList;
    }

    public Exercise setApproachList(ArrayList<Approach> approachList) {
        this.approachList = approachList;
        return this;
    }

    public Approach getApproach(int index) {
        if (approachList == null) return null;
        return approachList.get(index);
    }

    public Exercise addApproach(Approach approach) {
        if (approachList == null) approachList = new ArrayList<>();
        approachList.add(approach);
        return this;
    }

    public Exercise addApproach(int index, Approach approach) {
        if (approachList == null) approachList = new ArrayList<>();
        approachList.add(index, approach);
        return this;
    }

    public Exercise removeApproach(int index) {
        if (approachList == null) return this;
        approachList.remove(index);
        return this;
    }

    public int getLevel() {
        return level;
    }

    public Exercise setLevel(int level) {
        this.level = level;
        return this;
    }

    public String getLevelString() {
        switch (level) {
            case 1:
                return "Простая";
            case 2:
                return "Средняя";
            case 3:
                return "Высокая";
            default:
                return null;
        }
    }

    public String getTargetMuscle() {
        return targetMuscle;
    }

    public Exercise setTargetMuscle(String targetMuscle) {
        this.targetMuscle = targetMuscle;
        return this;
    }

    public String getInventory() {
        return inventory;
    }

    public Exercise setInventory(String inventory) {
        this.inventory = inventory;
        return this;
    }

    public String getInstruction() {
        return instruction;
    }

    public Exercise setInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }

    public ArrayList<String> getInvolvesMuscleArray() {
        String[] split = involvedMuscle.split(", ");
        ArrayList<String> array = new ArrayList<>(split.length);
        Collections.addAll(array, split);
        return array;
    }

    public String getInvolvedMuscle() {
        return involvedMuscle;
    }

    public Exercise setInvolvedMuscle(String involvedMuscle) {
        this.involvedMuscle = involvedMuscle;
        return this;
    }

    public String getAdvice() {
        return advice;
    }

    public Exercise setAdvice(String advice) {
        this.advice = advice;
        return this;
    }

    public int getStoreId() {
        return storeId;
    }

    public Exercise setStoreId(int storeId) {
        this.storeId = storeId;
        return this;
    }

    public boolean haveId() {
        return id > 0;
    }

    private int getMinReps() {
        try {
            int m = Integer.MAX_VALUE;
            for (Approach approach : getApproachList())
                if (m > approach.getReps()) m = approach.getReps();
            if (m == Integer.MAX_VALUE) return 0;
            return m;
        } catch (Exception e) {
            return 0;
        }
    }

    private int getMaxReps() {
        try {
            int m = Integer.MIN_VALUE;
            for (Approach approach : getApproachList())
                if (m < approach.getReps()) m = approach.getReps();
            if (m == Integer.MIN_VALUE) return 0;
            return m;
        } catch (Exception e) {
            return 0;
        }
    }

    public String getRepsString() {
        int min = getMinReps();
        int max = getMaxReps();
        if (min == max) return String.valueOf(max);
        if (max == 0) return String.valueOf(min);
        return min + "-" + max;
    }

    private int getMinWeight() {
        try {
            int m = Integer.MAX_VALUE;
            for (Approach approach : getApproachList())
                if (m > approach.getWeight()) m = approach.getWeight();
            if (m == Integer.MAX_VALUE) return 0;
            return m;
        } catch (Exception e) {
            return 0;
        }
    }

    private int getMaxWeight() {
        try {
            int m = Integer.MIN_VALUE;
            for (Approach approach : getApproachList())
                if (m < approach.getWeight())
                    m = approach.getWeight();
            if (m == Integer.MIN_VALUE)
                return 0;
            return m;
        } catch (Exception e) {
            return 0;
        }
    }

    public String getWeightString() {
        int min = getMinWeight();
        int max = getMaxWeight();
        if (min == max) return String.valueOf(max);
        if (max == 0) return String.valueOf(min);
        return min + "-" + max;
    }

    @Override
    public String toString() {
        return "id = " + getId() + ", storeId = " + getStoreId() + ", a_count = " + getApproachSize() + ", name = " + getName();
    }
}
