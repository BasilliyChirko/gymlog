package basilliy.gymlog.main;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import basilliy.gymlog.datamodel.Approach;
import basilliy.gymlog.datamodel.CursorWrapper;
import basilliy.gymlog.datamodel.Day;
import basilliy.gymlog.datamodel.Exercise;
import basilliy.gymlog.datamodel.Program;

public class DataBase {
    private SQLiteDatabase sql;
    private SQLHelper helper;
    private Context context;

    private DataBase(Context context) {
        this.context = context;
        helper = new SQLHelper(context, "gymlog_database", null, 1);
        sql = helper.getWritableDatabase();
    }

    public static DataBase with(Context context) {
        return new DataBase(context);
    }

    public void close() {
        sql.close();
    }


    public ArrayList<Exercise> getExerciseStore(String muscle) {
        SharedPreferences sp = context.getSharedPreferences(D.preferences.pref_name, Context.MODE_PRIVATE);
        String where = "level <= " + sp.getInt(D.preferences.level, 3);
        where += " and dumbbells <= " + sp.getInt(D.preferences.dumbbells, 1);
        where += " and barbell <= " + sp.getInt(D.preferences.barbell, 1);
        where += " and dumbbells_barbell <= " + sp.getInt(D.preferences.dumbbells_barbell, 1);
        where += " and power_bench <= " + sp.getInt(D.preferences.power_bench, 1);
        where += " and power_frame <= " + sp.getInt(D.preferences.power_frame, 1);
        where += " and rope_trainers <= " + sp.getInt(D.preferences.rope_trainers, 1);
        where += " and lever_trainers <= " + sp.getInt(D.preferences.lever_trainers, 1);
        where += " and other_trainers <= " + sp.getInt(D.preferences.other_trainers, 1);
        if (muscle != null) where += " and target_muscle like \"%" + muscle + "%\"";

        Cursor cursor = sqlQuery("exercise_store", where);
        ArrayList<Exercise> aExercise = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                aExercise.add(readExerciseStore(cursor));
            } while (cursor.moveToNext());
        }
        return aExercise;
    }

    public Exercise getExerciseStore(int id) {
        Cursor cursor = sqlQuery("exercise_store", "id = " + id);
        if (cursor.moveToFirst())
            return readExerciseStore(cursor);
        return null;
    }

    private Exercise readExerciseStore(Cursor cursor) {
        Exercise e = new Exercise();
        e.setStoreId(cursor.getInt(cursor.getColumnIndex("id")));
        e.setName(cursor.getString(cursor.getColumnIndex("name")));
        e.setTargetMuscle(cursor.getString(cursor.getColumnIndex("target_muscle")));
        e.setInvolvedMuscle(cursor.getString(cursor.getColumnIndex("involved_muscle")));
        e.setLevel(cursor.getInt(cursor.getColumnIndex("level")));
        e.setInventory(cursor.getString(cursor.getColumnIndex("inventory")));
        e.setDescription(cursor.getString(cursor.getColumnIndex("description")));
        e.setAdvice(cursor.getString(cursor.getColumnIndex("advice")));
        e.setInstruction(cursor.getString(cursor.getColumnIndex("how_to_do")));
        return e;
    }







    /***
     * ---------------------------------------------------------------------------------------------
     * Работа с программой треннировок
     * ---------------------------------------------------------------------------------------------
     */



    //-------Обновление Программы
    public DataBase updateProgram(Program program) {
        try {
            sql.beginTransaction();
            deleteProgram(program);
            addProgram(program);
            sql.setTransactionSuccessful();
        } finally {
            sql.endTransaction();
        }
        return this;
    }

    //-------Удаление Программы
    public DataBase deleteProgram(Program program) {
        sqlDelete("program", "id = " + program.getId());
        for (Day day : program.getDayList()) {
            sqlDelete("day", "id = " + day.getId());
            for (Exercise exercise : day.getExerciseList()) {
                sqlDelete("exercise", "id = " + exercise.getId());
                for (Approach approach : exercise.getApproachList()) {
                    sqlDelete("approach", "id = " + approach.getId());
                }
            }
        }
        return this;
    }

    //-------Добавление Программы
    public DataBase addProgram(Program program) {
        ContentValues v = new ContentValues();
        v.put("name", program.getName());
        v.put("level", program.getLevel());
        v.put("male", program.getMale());

        if (program.getDescription() != null)
            v.put("description", program.getDescription());
        if (program.getTarget() != null)
            v.put("target", program.getTarget());
        if (program.getBodyType() != null)
            v.put("body_type", program.getBodyType());
        if (program.haveId())
            v.put("id", program.getId());

        sqlInsert("program", v);
        if (!program.haveId()) {
            Cursor cursor = sql.rawQuery("select max(id) as id from program", null);
            cursor.moveToFirst();
            program.setId(cursor.getInt(cursor.getColumnIndex("id")));
            cursor.close();
        }
        setDayList(program);
        return this;
    }

    private void setDayList(Program program) {
        for (int i = 0; i < program.getDayList().size(); i++) {
            Day day = program.getDay(i);
            ContentValues v = new ContentValues();
            v.put("id_program", program.getId());
            v.put("number", day.getNumber());
            v.put("name", day.getName());
            if (day.haveId())
                v.put("id", day.getId());
            sqlInsert("day", v);
            if (!day.haveId()) {
                Cursor cursor = sql.rawQuery("select max(id) as id from day", null);
                cursor.moveToFirst();
                day.setId(cursor.getInt(cursor.getColumnIndex("id")));
                cursor.close();
            }
            setExerciseList(day);
        }
    }

    private void setExerciseList(Day day) {
        ArrayList<Exercise> exerciseList = day.getExerciseList();
        for (int i = 0; i < exerciseList.size(); i++) {
            Exercise exercise = exerciseList.get(i);
            ContentValues v = new ContentValues();
            v.put("id_day", day.getId());
            v.put("id_exercise_store", exercise.getStoreId());
            v.put("number", i);
            if (exercise.haveId())
                v.put("id", exercise.getId());
            sqlInsert("exercise", v);
            if (!exercise.haveId()) {
                Cursor cursor = sql.rawQuery("select max(id) as id from exercise", null);
                cursor.moveToFirst();
                exercise.setId(cursor.getInt(cursor.getColumnIndex("id")));
                cursor.close();
            }
            setApproachList(exercise);
        }
    }

    private void setApproachList(Exercise exercise) {
        ArrayList<Approach> approachList = exercise.getApproachList();
        for (int i = 0; i < approachList.size(); i++) {
            ContentValues v = new ContentValues();
            v.put("id_exercise", exercise.getId());
            v.put("reps", approachList.get(i).getReps());
            v.put("weight", approachList.get(i).getAbsWeight());
            v.put("number", i);
            if (approachList.get(i).haveId())
                v.put("id", approachList.get(i).getId());
            sqlInsert("approach", v);
        }
    }


    //-------Получение Программы
    public Program getProgram(int id) {
        Cursor cursor = sqlQuery("program", "id = " + id);
        if (cursor.moveToFirst()) {
            Program program = reedProgram(cursor);
            cursor.close();
            return program;
        }
        cursor.close();
        close();
        return null;
    }

    public ArrayList<Program> getAllProgram() {
        Cursor cursor = sqlQuery("program", null);
        ArrayList<Program> programList = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                programList.add(reedProgram(cursor));
            } while (cursor.moveToNext());
        }
        return programList;
    }

    private Program reedProgram(Cursor cursor) {
        Program program = new Program();
        program.setId(cursor.getInt(cursor.getColumnIndex("id")));
        program.setDescription(cursor.getString(cursor.getColumnIndex("description")));
        program.setName(cursor.getString(cursor.getColumnIndex("name")));
        program.setTarget(cursor.getString(cursor.getColumnIndex("target")));
        program.setBodyType(cursor.getString(cursor.getColumnIndex("body_type")));
        program.setLevel(cursor.getInt(cursor.getColumnIndex("level")));
        program.setMale(cursor.getInt(cursor.getColumnIndex("male")));
        program.setDayList(getDayList(program));
        return program;
    }

    private ArrayList<Day> getDayList(Program program) {
        Cursor cursor = sqlQuery("day", "id_program = " + program.getId(), "number");
        ArrayList<Day> dayList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Day day = new Day();
                day.setId(cursor.getInt(cursor.getColumnIndex("id")));
                day.setNumber(cursor.getInt(cursor.getColumnIndex("number")));
                day.setName(cursor.getString(cursor.getColumnIndex("name")));
                day.setExerciseList(getExerciseList(day));
                dayList.add(day);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return dayList;
    }

    private ArrayList<Exercise> getExerciseList(Day day) {
        Cursor cursor = sql.rawQuery(
                        "select " +
                        "e.id, " +
                        "e.number, " +
                        "e.id_exercise_store, " +
                        "es.name, " +
                        "es.description, " +
                        "es.target_muscle, " +
                        "es.involved_muscle, " +
                        "es.inventory, " +
                        "es.level, " +
                        "es.how_to_do, " +
                        "es.advice " +
                        "from exercise as e  " +
                        "inner join exercise_store as es on e.id_exercise_store = es.id " +
                        "where e.id_day = " + day.getId(), null);
        CursorWrapper w = new CursorWrapper(cursor);
        ArrayList<Exercise> exerciseList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                Exercise e = new Exercise();
                e.setId(cursor.getInt(cursor.getColumnIndex("id")));
                e.setName(cursor.getString(cursor.getColumnIndex("name")));
                e.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                e.setTargetMuscle(cursor.getString(cursor.getColumnIndex("target_muscle")));
                e.setInvolvedMuscle(cursor.getString(cursor.getColumnIndex("involved_muscle")));
                e.setInventory(cursor.getString(cursor.getColumnIndex("inventory")));
                e.setLevel(cursor.getInt(cursor.getColumnIndex("level")));
                e.setInstruction(cursor.getString(cursor.getColumnIndex("how_to_do")));
                e.setAdvice(cursor.getString(cursor.getColumnIndex("advice")));
                e.setStoreId(cursor.getInt(cursor.getColumnIndex("id_exercise_store")));
                e.setApproachList(getApproachList(e));
                exerciseList.add(e);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return exerciseList;
    }

    private ArrayList<Approach> getApproachList(Exercise exercise) {
        Cursor cursor = sqlQuery("approach", "id_exercise = " + exercise.getId(), "number");
        ArrayList<Approach> approachList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Approach approach = new Approach();
                approach.setId(cursor.getInt(cursor.getColumnIndex("id")));
                approach.setReps(cursor.getInt(cursor.getColumnIndex("reps")));
                approach.setWeight(cursor.getFloat(cursor.getColumnIndex("weight")));
                approachList.add(approach);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return approachList;
    }




    public void test() {
        Cursor cursor = sql.rawQuery("select * from exercise", null);
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                int store = cursor.getInt(cursor.getColumnIndex("id_exercise_store"));
                D.log("id = " + id + ", store = " + store);
            } while (cursor.moveToNext());
        }
        cursor.close();

    }

//    public void foo() {
//        ContentValues v = new ContentValues();
//        v.put("id", 1);
//        v.put("name", "program1");
//        v.put("description", "description1");
//        v.put("days", 5);
//        sqlInsert("program", v);
//
//        v = new ContentValues();
//        v.put("id_program", 1);
//        v.put("name", "day_1.1");
//        v.put("number", 1);
//        sqlInsert("day", v);
//        v.put("id_program", 1);
//        v.put("name", "day_1.2");
//        v.put("number", 2);
//        sqlInsert("day", v);
//
//        v = new ContentValues();
//        v.put("id_day", 1);
//        v.put("number", 1);
//        v.put("id_exercise_store", 1);
//        sqlInsert("exercise", v);
//        v.put("id_day", 1);
//        v.put("number", 2);
//        v.put("id_exercise_store", 2);
//        sqlInsert("exercise", v);
//        v.put("id_day", 2);
//        v.put("number", 1);
//        v.put("id_exercise_store", 1);
//        sqlInsert("exercise", v);
//        v.put("id_day", 2);
//        v.put("number", 2);
//        v.put("id_exercise_store", 2);
//        sqlInsert("exercise", v);
//        v.put("id_day", 2);
//        v.put("number", 3);
//        v.put("id_exercise_store", 3);
//        sqlInsert("exercise", v);
//
//        v = new ContentValues();
//        v.put("id_exercise", 1);
//        v.put("number", 0);
//        v.put("count", 4);
//        v.put("value", 10);
//        sqlInsert("approach", v);
//
//        v.put("id_exercise", 2);
//        v.put("number", 0);
//        v.put("count", 4);
//        v.put("value", 20);
//        sqlInsert("approach", v);
//
//        v.put("id_exercise", 3);
//        v.put("number", 0);
//        v.put("count", 5);
//        v.put("value", 30);
//        sqlInsert("approach", v);
//
//        v.put("id_exercise", 4);
//        v.put("number", 0);
//        v.put("count", 5);
//        v.put("value", 40);
//        sqlInsert("approach", v);
//
//        v.put("id_exercise", 5);
//        v.put("number", 0);
//        v.put("count", 15);
//        v.put("value", 10);
//        sqlInsert("approach", v);
//
//        v.put("id_exercise", 5);
//        v.put("number", 1);
//        v.put("count", 16);
//        v.put("value", 20);
//        sqlInsert("approach", v);
//
//        v.put("id_exercise", 5);
//        v.put("number", 2);
//        v.put("count", 17);
//        v.put("value", 30);
//        sqlInsert("approach", v);
//
//
//
//
//
//    }

    public DataBase reset() {
        //Удаление таблиц
        sql.execSQL("DROP TABLE IF EXISTS program");
        sql.execSQL("DROP TABLE IF EXISTS day");
        sql.execSQL("DROP TABLE IF EXISTS exercise");
        sql.execSQL("DROP TABLE IF EXISTS approach");
        sql.execSQL("DROP TABLE IF EXISTS exercise_store");


        //Создание таблиц и установление данных по умолчанию
        helper.onCreate(sql);

        return this;
    }



    private Cursor sqlQuery(String table, String where) {
        return sql.query(table, null, where, null, null, null, null);
    }

    private Cursor sqlQuery(String table, String where, String order) {
        return sql.query(table, null, where, null, null, null, order);
    }

    private void sqlUpdate(String table, String where, ContentValues values) {
        sql.update(table, values, where, null);
    }

    private void sqlDelete(String table, String where) {
        sql.delete(table, where, null);
    }

    private void sqlInsert(String table, ContentValues values) {
        sql.insert(table, null, values);
    }


    private class SQLHelper extends SQLiteOpenHelper {

        public SQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL("CREATE TABLE program( " +
                    "id integer primary key autoincrement, " +
                    "name text, " +
                    "description text default null, " +
                    "target text default null, " +
                    "body_type text default null, " +
                    "level integer default -1, " +
                    "male integer default -1 " +
                    ");");

            sqLiteDatabase.execSQL("CREATE TABLE day( " +
                    "id integer primary key autoincrement, " +
                    "id_program integer, " +
                    "name text, " +
                    "number integer);");

            sqLiteDatabase.execSQL("CREATE TABLE exercise( " +
                    "id integer primary key autoincrement, " +
                    "id_day integer, " +
                    "number integer, " +
                    "id_exercise_store integer);");

            sqLiteDatabase.execSQL("CREATE TABLE approach( " +
                    "id integer primary key autoincrement, " +
                    "id_exercise integer, " +
                    "reps integer, " +
                    "number integer, " +
                    "weight float default -1);");

            sqLiteDatabase.execSQL("CREATE TABLE exercise_store( " +
                    "id integer primary key autoincrement, " +
                    "name text, " +
                    "description text, " +
                    "target_muscle text, " +
                    "involved_muscle text, " +
                    "inventory text, " +
                    "level integer, " +
                    "how_to_do text, " +
                    "advice text, " +
                    "dumbbells integer default 0, " + //Гантели
                    "barbell integer default 0, " + //Штанга
                    "dumbbells_barbell integer default 0, " + //Гантели-Штанга
                    "power_bench integer default 0, " + //Силовая скамья
                    "power_frame integer default 0, " + //Силовая рама
                    "rope_trainers integer default 0, " + //Тросовые тренажеры
                    "lever_trainers integer default 0, " + //Рычажные тренажеры
                    "other_trainers integer default 0 " + //Другие тренажеры
                    ");");

            sql = sqLiteDatabase;
            setDefaultExercise(sqLiteDatabase);
//            setDefaultProgram(sqLiteDatabase);
            sql = null;
        }

        private void setDefaultProgram(SQLiteDatabase db) {
            try {
                db.beginTransaction();
                Program p;
                Day d;
                Exercise e;
                Approach a;


                //Программа 1
                p = new Program();
                p.setLevel(2);
                p.setName("Программа тренировок для набора мышечной массы");
                p.setDescription("Всё что необходимо для быстрого массанабора");
                p.setTarget("Набор массы");
                p.setBodyType("Любой");
                p.setMale(Program.MALE);

                d = new Day();
                p.addDay(d);
                d.setNumber(0);
                d.setName("Грудь и Бицепс");
                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(70);
                e.addApproach(new Approach().setReps(9).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(7).setWeight(66));
                e.addApproach(new Approach().setReps(6).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(20);
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(9).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(66);
                e.addApproach(new Approach().setReps(12).setWeight(66));
                e.addApproach(new Approach().setReps(11).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(1);
                e.addApproach(new Approach().setReps(9).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(7).setWeight(66));
                e.addApproach(new Approach().setReps(6).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(43);
                e.addApproach(new Approach().setReps(9).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(7).setWeight(66));
                e.addApproach(new Approach().setReps(6).setWeight(66));

                d = new Day();
                d.setNumber(1);
                p.addDay(d);

                d = new Day();
                p.addDay(d);
                d.setNumber(2);
                d.setName("Спина и Плечи");
                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(68);
                e.addApproach(new Approach().setReps(9).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(7).setWeight(66));
                e.addApproach(new Approach().setReps(6).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(73);
                e.addApproach(new Approach().setReps(12).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(6).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(64);
                e.addApproach(new Approach().setReps(12).setWeight(66));
                e.addApproach(new Approach().setReps(11).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(63);
                e.addApproach(new Approach().setReps(12).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(6).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(62);
                e.addApproach(new Approach().setReps(12).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(6).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(39);
                e.addApproach(new Approach().setReps(12).setWeight(66));
                e.addApproach(new Approach().setReps(11).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));


                d = new Day();
                d.setNumber(3);
                p.addDay(d);

                d = new Day();
                p.addDay(d);
                d.setNumber(4);
                d.setName("Ноги и Трицепс");
                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(52);
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(9).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(7).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(60);
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(9).setWeight(66));
                e.addApproach(new Approach().setReps(8).setWeight(66));
                e.addApproach(new Approach().setReps(7).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(51);
                e.addApproach(new Approach().setReps(12).setWeight(66));
                e.addApproach(new Approach().setReps(11).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(9).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(61);
                e.addApproach(new Approach().setReps(12).setWeight(66));
                e.addApproach(new Approach().setReps(11).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));

                e = new Exercise();
                d.addExercise(e);
                e.setStoreId(35);
                e.addApproach(new Approach().setReps(12).setWeight(66));
                e.addApproach(new Approach().setReps(11).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));
                e.addApproach(new Approach().setReps(10).setWeight(66));

                addProgram(p);

                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
        }

        private void setDefaultExercise(SQLiteDatabase db) {
            try {
                db.beginTransaction();
                ContentValues values = new ContentValues();

//exercise = 1
                values.put("name", "Сгибание рук со штангой стоя");
                values.put("target_muscle", "Бицепс");
                values.put("involved_muscle", "Предплечья");
                values.put("level", 1);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Данное упражнение самое популярное для мышц бицепса. Существует несколько вариаций - может выполняться со штангой (как с прямым, так и с изогнутым грифом) и с гантелями.");
                values.put("how_to_do", "1. Встаньте прямо, возьмите штангу хватом снизу. Исходное положение: руки на ширине плеч, локти близко к туловищу.\n2. На выдохе - удерживая неподвижно верхние отделы рук (от локтя до плеча) выполните сгибание рук в локтях усилием бицепсов. Работают только предплечья.\n3. Сгибание должно продолжаться до момента, когда штанга не будет находится на уровне плеч. В верхней точке стоит задержаться, напрягая при этом бицепс.\n4. На вдохе - медленно опустите штангу в исходное положение.\n5. Повторите упражнение необходимое число раз.");
                values.put("advice", "1. Ширина хвата имеет большое значение: если хват уже плеч - то нагрузка больше идет на внешнюю головку бицепса, на ширине плеч и более - на внутреннюю горовку.\n2. Разгибать руки в локтях необходимо до конца. А полностью сгибать - не стоит. Кисть в верхней точке должна быть немного впереди локтя. В этом случае в верхней точке бицепс будет максимально напряжён.\n3. При подъёме штанги локти следует совсем немного выводить вперёд - это позволит чтобы сильнее сократить бицепсы.\n4. Спину необходимо держать прямо, а плечи отвести немного назад. Не стоит раскачиваться или помогать себе спиной или ногами - лучше попросите подстраховать.");
                db.insert("exercise_store", null, values);

//exercise = 2
                values.put("name", "Сгибание рук на скамье Скотта");
                values.put("target_muscle", "Бицепс");
                values.put("involved_muscle", "Предплечья");
                values.put("level", 1);
                values.put("inventory", "Другие тренажеры, Штанга");
                values.put("barbell", 1); //Штанга
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Скамья Скотта заставляет мышцы бицепсов работать изолированно и позволяет достичь максимальной нагрузки. Опускать штангу вниз необходимо как можно медленней, чтобы избежать риск получения травмы локтя. Упражнение обычно выполняют с штангой с EZ-грифом (можно использовать гантели).");
                values.put("how_to_do", "1. Сядьте на скамью Скотта, возьмите штангу. Плотно обопритесь грудью и локтями в подставку.\n2. Исходное положение - руки согнуты в локтях, ладони находятся на уровне плеч и направлены вперед.\n3. На вдохе - медленно опускайте штангу, выпрямляя руки.\n4. На выдохе, за счет напряжения бицепсов, сгибайте руки до полного сокращения бицепсов, при этом штанга должна оказаться на уровне плеч. Ненадолго задержитесь в верхней точке.\n5. Выполните необходимое количество повторений.");
                values.put("advice", "1. Не выпрямляйте руки полностью для снижения риска получения травмы.\n2. Перед выполнением упражнения необходимо отрегулировать скамью под себя: верхняя часть упора должна была примерно на середине груди или немного ниже.\n3. Упражнение рекомендуется выполнять с ассистентом. Снимая штангу можно потянуть сухожилия.");
                db.insert("exercise_store", null, values);

//exercise = 3
                values.put("name", "Сгибание шеи лёжа на скамье");
                values.put("target_muscle", "Шея");
                values.put("involved_muscle", "-");
                values.put("level", 2);
                values.put("inventory", "Силовая скамья");
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Одно из базовых упражнений для тренировки шеи. Выполняя упражнение необходимо разгибать и сгибать шею с полной амплитудой, плавно, без резких движений. Диск берите такой, чтобы вы могли выполнить хотя бы 10 повторений.");
                values.put("how_to_do", "1. Лягте на скамью на спину так, чтобы плечи немного выступали за край скамьи.\n2. Диск положите на лоб, и во время выполнения упражнения придерживайте его руками.\n3. На вдохе - на полную амплитуда опустите голову вниз.\n4. На выдохе - за счет напряжения мышц шеи поднимите голову вверх, немного выше горизонтального положения.\n5. Повторите упражнение необходимое количество раз.");
                values.put("advice", "1. Если нет диска, то можете воспользоваться другими тяжестями или попросить товарища давить вам на лоб руками. В крайнем случае - возьмите полотенце, положите его на лоб, и тяните за края вниз.\n2. Если вы раньше не тренировали шею, то для начала Вам достаточно диска весом 2.5 - 5 кг.\n3. Следите за амплитудой и плавностью движений. Для большего эффекта можете немного задержаться в верхней точке.\n4. Выполняйте не только сгибание шеи, но и разгибание, для равномерного развития мышц.");
                db.insert("exercise_store", null, values);

//exercise = 4
                values.put("name", "Разгибание шеи лёжа на скамье");
                values.put("target_muscle", "Шея");
                values.put("involved_muscle", "-");
                values.put("level", 2);
                values.put("inventory", "Силовая скамья");
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Популярное упражнение для мышц шеи. Опускать и поднимать голову необходимо до упора. От скамьи грудь не отрывать. Выполнять медленно, резких движений не делать. Вес как и для сгибания шеи на скамье стоит выбирать такой, чтобы выполнить хотя бы 10 повторений.");
                values.put("how_to_do", "1. Исходное положение: лягте грудью на скамью так, чтобы верхняя половина груди выступала за край скамьи. Груз держите руками на затылке.\n2. На вдохе - опустите голову вниз, как бы кивая.\n3. На выдохе - поднимите голову вверх чуть выше горизонтального положения.\n4. Повторите необходимое число раз");
                values.put("advice", "1. Если нет диска, то можете воспользоваться другими тяжестями или попросить товарища давить вам на затылок руками. В крайнем случае - возьмите полотенце, положите его на лоб, и тяните за края вниз. Также существует специальное приспособление, одевающееся на голову.\n2. Максимально плавно опускать и поднимать голову. Для большего эффекта следует задержаться на секунду в верхней точке.\n3. Начните выполнять упражнение с небольшими весами. Попробуйте диски по 2.5 - 5 кг на 15 повторений. Даже этого веса вполне будет достаточно, чтобы загрузить нетренированные мышцы.\n4. Не стоит сильно задирать голову вверх - это опасно для здоровья.");
                db.insert("exercise_store", null, values);

//exercise = 5
                values.put("name", "Жим штанги из-за головы стоя или сидя");
                values.put("target_muscle", "Плечи");
                values.put("involved_muscle", "Трицепс");
                values.put("level", 2);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Выполняется как стоя, так и сидя. Возможно выполнение в машине Смита. Т.к. штанга опускается за голову и хват шире плеч, то кроме передней дельты активно работает 2.");
                values.put("how_to_do", "1. Возьмите штангу в руки хватом чуть шире плеч.\n2. Примите исходное положение: штанга лежит на плечах. Спина прямая.\n3. На выдохе - поднимите медленно штангу вверх, полностью выпрямив руки.\n4. На вдохе - опустите штангу за голову примерно до уровня глаз.\n5. Повторите упражнение необходимое количество раз.");
                values.put("advice", "1. Сохраняйте туловище неподвижным, а спину прямой.\n2. Штанга всегда должна быть на весу. Не кладите её на плечи.\n3. Вес штанги нужно подбирать такой, чтобы можно было выполнить минимум 8 повторений.\n4. Упражнение лучше выполнять возле стойки - так брать снаряд удобнее. Идеально расположение штанги чуть ниже уровня плеч - чтобы можно было зайти под штангу.\n5. Ширина хвата для правильного выполнения: предплечья в нижней точке параллельны друг другу (строго вертикальны).\n6. Удобней и безопасней выполнять упражнение в машине Смита (и стоя и сидя).\n7. При выполнении жима за голову сидя опирайтесь на спинку.");
                db.insert("exercise_store", null, values);

//exercise = 6
                values.put("name", "Шраги с гантелями стоя");
                values.put("target_muscle", "Трапеции");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "От похожего упражнения со штангой отличается тем, что используются гантели, с которыми выполнять упражнение более удобно.");
                values.put("how_to_do", "1. Исходное положение: гантели в каждой руке, плечи расправлены, руки выпрямлены вдоль тела, ладонями внутрь, подбородок прижат к груди.\n2. На вдохе - поднимите вверх гантели, образно стараясь соединить плечи на затылке. В верхней точке немного задержитесь.\n3. На выдохе - опустите плечи в исходное положение.\n4. Повторите необходимое число раз.");
                values.put("advice", "1. Выполняйте упражнение с полной амплитудой. Максимально расслабляйте мышцы внизу и делая паузу в верху.\n2. Не сгибайте руки в локтях. Не используйте бицепсы в этом упражнении. Двигайте вверх и вниз за счет усилия трапеций.\n3. Попробуйте не только поднимать плечи вверх, но и немного отводить их назад. Так трапециевидные мышцы работают сильнее.");
                db.insert("exercise_store", null, values);

//exercise = 7
                values.put("name", "Разгибание запястий сидя");
                values.put("target_muscle", "Предплечья");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Гантели, Штанга");
                values.put("dumbbells_barbell", 1); //Гантели-Штанга
                values.put("description", "Выберите для себя удобное положение - руки можно класть на лавку или себе на колени, но в любом случае отрывать локти от подставки не нужно. Особенно важно это упражнение для начинающих, т.к. у них мышцы предплечья, как правило, отстают.");
                values.put("how_to_do", "1. Штангу взять хватом сверху, в замок. Положите предплечья себе на бедра или на лавку. Опустите запястья вниз. Это исходное положение.\n2. На выдохе - разогните запястья по направлению к себе, поднимая штангу вверх.\n3. На вдохе - опускайте штангу в исходное положение.\n4. Выполните необходимое число повторений.");
                values.put("advice", "1. Хват штанги такой, чтобы предплечья были параллельны друг другу.\n2. Можно выполнять разгибание запястий с гантелями вместо штанги. Но с гантелями упражнение менее эффективно - т.к. кисти в этом случае могут отклоняться в стороны, что приводит к снижению эффективности упражнения.\n3. Немного задерживайтесь в верхней точке, чтобы увеличить нагрузку на предплечья.\n4. Локти удерживайте неподвижными - не отрывайте их от опоры.\n5. Подбирайте вес такой, чтобы повторить упражнение хотя бы 10 раз.");
                db.insert("exercise_store", null, values);

//exercise = 8
                values.put("name", "Отжимания на брусьях");
                values.put("target_muscle", "Трицепс");
                values.put("involved_muscle", "Плечи, Грудь");
                values.put("level", 1);
                values.put("inventory", "Брусья");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Самое распространенное упражнение для прокачки трицепса. Отличительная особенность - можно выполнять практически в любом дворе :). Оптимальная ширина хвата - ширина Ваших плечей.");
                values.put("how_to_do", "1. Исходное положение - на брусьях удерживайте тело на вытянутых руках.\n2. На вдохе - опуститесь вниз. Тело держите прямо, локти как можно ближе к телу. В нижней точке руки должны быть согнуты в локтях под прямым углом.\n3. На выдохе - усилием трицепса вернитесь в исходное положение.\n4. Выполните нужное количество повторений.");
                values.put("advice", "1. Если у Вас недостаточно сил для отжиманий на брусьях используйте специальный тренажер, с противовесом. Если такого тренажера нет, то подойдет напарник, который будет подталкивать за ноги.\n2. Более опытные атлеты могут применять утяжеления. Вес при этом стоит выбирать такой, чтобы выполнить хотя бы 5-6 повторений. Но всё равно первый подход делайте без веса в качестве разминки\n3. Чтобы сильнее нагрузить трицепс, брусья - поуже, а локти отводить назад. А если хотите сильнее нагрузить грудь, брусья - пошире, а локти разводить в стороны.");
                db.insert("exercise_store", null, values);

//exercise = 9
                values.put("name", "Пуловер лежа с гантелей");
                values.put("target_muscle", "Грудь");
                values.put("involved_muscle", "Плечи, Трицепс, Широчайшие мышцы спины");
                values.put("level", 2);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Некоторые считают, что это упражнение для мышц груди. Бесспорно, грудь здесь тоже работает, но этим упражнением вы ее не накачаете. Эффективность загрузки мышц зависит от того, насколько опускаются локти, а не сам снаряд, поэтому сильно сгибать локти смысла нет. Упражнение лучше делать в конце тренировки - с целью растянуть мышцы. Выполнять можно с гантелей (можно сразу с двумя), штангой или диском.");
                values.put("how_to_do", "1. Лягте спиной на скамью (затылок на краю скамьи). Ноги согните в коленях.\n2. Возьмите гантель двумя руками таким образом, чтобы гриф был зажат между большими пальцами рук, а другие пальцы обхватывали диски гантели и зафиксируйте её над грудью на прямых вытянутых руках. Руки выпрямлены перпендикулярно телу.\n3. На вдохе - не сгибая руки в локтях, медленно опускайте гантель за голову - чуть ниже горизонтального положения.\n4. На выдохе - поднимите гантель в исходную позицию и задержитесь на секунду.\n5. Выполнение необходимое количество провторений.");
                values.put("advice", "1. Если вы новичок в пуловере, то лучше будет, если кто-нибудь подстрахует Вас.\n2. Данное упражнение также выполняют поперёк лавки. Но видимых преимуществ не замечено, только неудобства.\n3. Руки следует сгибать внизу совсем немного - иначе пуловер превращается во французский жим.\n4. В этом упражнении активно работают грудь, трицепсы и спина. Эффективно накачать плечи пуловером вы не сможете, но зато отлично сможете растянуть мышцы в конце тренировки. Также пуловер рекомендуется в подростковом возрасте для расширения грудной клетки.\n5. Правильная амплитуда для данного упражнения - полукруг, рисуемый локтями.");
                db.insert("exercise_store", null, values);

//exercise = 10
                values.put("name", "Скручивания лёжа");
                values.put("target_muscle", "Пресс");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Отсутствует");
                values.put("description", "Скручивание - базовое упражнений для формирования пресса, направлено в основном на прямую мышцу брюшной области. При выполнении упражнения главное - скрутиться, а не высоко подниматься. Так же можно поворачивать туловище, касаясь локтем колена.");
                values.put("how_to_do", "1. Исходное положение: лежа на спине, упритесь ступнями в пол, колени согнуты под углом в 90 градусов. Руки заведены за голову, локти вовнутрь.\n2. На выдохе - начинайте скручивание. Упираясь поясницей в пол поднимайте плечи. Отрывайте их не больше чем на 10 см, поясница при этом остается прижатой к полу. В конечной точке задержитесь и напрягите пресс.\n3. На вдохе - медленно вернитесь в первоначальное положение.\n4. Выполните необходимое количество повторений.");
                values.put("advice", "1. Можно делать как с согнутыми ногами, так и с прямыми. С согнутыми - чуть сложнее, потому что при сгибании ног уже немного сокращается пресс. Удобнее будет засунуть ноги под какую-нибудь опору или попросить чтобы их кто-то держал.\n2. При выполнении можно использовать утяжеление (диск от штанги).\n3. Старайтесь скрутиться по максимуму. Представьте, что хотите коснуться лбом коленей. Но при этом не отрывайте поясницу от пола, чтобы не включать в работу поясничную мышцу.");
                db.insert("exercise_store", null, values);

//exercise = 11
                values.put("name", "Сгибания рук стоя или сидя у верхних блоков");
                values.put("target_muscle", "Бицепс");
                values.put("involved_muscle", "-");
                values.put("level", 2);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "Сгибание рук у верхних блоков - это изолирующее дополнительное упражнение, направленное на проработку бицепса. Оно может выполняться как в положении стоя, так и сидя.");
                values.put("how_to_do", "1. Установите подходящий Вам вес (одинаковый) с двух сторон тросового тренажера. Настройте тросы так, чтобы рукояти были выше Ваших плеч. Займите центральное положение в раме и возьмите рукояти таким хватом, чтобы ладони были направлены в сторону потолка.\n2. Исходное положение: поставьте ноги на ширине плеч, а тело держите на одной прямой с рукоятями.\n3. На выдохе - держа туловище неподвижно, плавно сгибайте руки до тех пор, пока предплечье не дотронется до бицепса.\n4. На вдохе - распрямите руки в локте.\n5. Выполните требуемое количество повторений.");
                values.put("advice", "1. Выполняя упражнение, следите за Вашими локтями. Не позволяйте им выходить вперед, так как в этом случае нагрузка с бицепса перейдет на грудь.\n2. Если выполнять упражнение, в положении сидя, то плечи будут находиться ниже уровня блока. Таким образом, бицепс будет сокращаться сильнее. \n3. Возможен вариант исполнения этого упражнения одной рукой. В таком случае Вам необходимо зафиксировать равновесие, взявшись свободной рукой за раму кроссовера.");
                db.insert("exercise_store", null, values);

//exercise = 12
                values.put("name", "Наклоны с гантелей в сторону стоя");
                values.put("target_muscle", "Пресс");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Наклоны с гантелей в сторону стоя является изолирующим упражнением, основная цель которого, проработать косые мышцы живота. Оно подойдет как для новичков, так и для профессиональных спортсменов.");
                values.put("how_to_do", "1. Исходное положение: поставьте ноги на ширине плеч и возьмите гантель правой рукой так, чтобы ладонь была направлена к туловищу. Левую руку поставьте на талию.\n2. На вдохе - держа спину ровно, сделайте наклон, сгибаясь в талии влево, максимально низко. В нижней точке задержитесь на секунду.\n3. На выдохе - плавно вернитесь в исходное положение.\n4. Выполните необходимое количество повторений каждой из рук.");
                values.put("advice", "1. Выполняйте упражнение строго в бок, не выдвигая тело вперед, так как эффективность упражнения сильно уменьшится.\n2. Старайтесь зафиксировать таз на месте, и делать наклоны исключительно сгибаясь в талии.\n3. Не устанавливайте слишком большие веса на гантели, поскольку есть вероятность увеличить талию.\n4. Не стоит использовать одновременно две гантели. Так Вы создадите противовес друг для друга и снизите эффективность упражнения до нуля.");
                db.insert("exercise_store", null, values);

//exercise = 13
                values.put("name", "Французский жим с гантелями сидя");
                values.put("target_muscle", "Трицепс");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Гантели, Силовая скамья");
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Жим гантели из-за головы сидя считается одним из самых популярных упражнений на трицепс. В большей степени оно подходит для новичков, так как из-за своих особенностей более удобно, чем жим стоя.;");
                values.put("how_to_do", "1. Для выполнения этого упражнения Вам понадобится горизонтальная скамья со спинкой. Сядьте на скамью и возьмите гантель обеими руками за основание.\n2. Исходное положение: удерживайте гантель на вытянутых руках так, чтобы часть руки от плеча до локтя была перпендикуляра полу и находилась около Вашей головы.\n3. На вдохе - плавно опускайте гантель за голову до тех пор, пока Ваши предплечья не коснутся бицепсов.\n4. На выдохе - напрягая трицепсы, сделайте подъем гантелей, вернувшись в исходное положение.\n5. Сделайте требуемое количество повторений.");
                values.put("advice", "1. Для новичков возможен вариант исполнения этого упражнения с диском от штанги вместо гантели.\n2. На протяжении всего упражнения не давайте локтям гулять в разные стороны, так как это сильно снижает эффективность упражнения.\n3. Подбирайте отягощение так, чтобы Вы могли сделать не менее 8 повторений.");
                db.insert("exercise_store", null, values);

//exercise = 14
                values.put("name", "Обратные отжимания");
                values.put("target_muscle", "Трицепс");
                values.put("involved_muscle", "Грудь, Плечи");
                values.put("level", 1);
                values.put("inventory", "Силовая скамья");
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Это эффективное упражнение, направленное на проработку трицепса. Также небольшая нагрузка поступает на верхнюю дельту и большую грудную мышцу. Такие отжимания рекомендуются для тех, кто еще не научился отжиматься от брусьев.");
                values.put("how_to_do", "1. В качестве инвентаря для данного упражнения необходимо использовать одну или две скамьи.\n2. Исходное положение: сядьте на край скамьи и поставьте руки шире ширины плеч. Упритесь ладонями в край скамьи, расположив ноги пятками на второй лавке.\n3. На вдохе - медленно опускайте таз вниз, сгибая руки в локтях до прямого угла. Держите спину ровно, не выпячивая таз вперед.\n4. На выдохе - используя трицепс, плавно выполните подъем тела вверх к исходному положению. В верхней точке упражнения сделайте небольшую паузу.\n5. Сделайте требуемое количество повторений.");
                values.put("advice","1. Это упражнение можно выполнять, постепенно увеличивая сложность. Самый простой вариант этого упражнения - поставить ноги на пол, сгибая их в коленях. После этого возможно использование лавки и своего веса. Наиболее эффективным упражнение будет, когда Вы кладете блины от штанги себе на бедра.\n2. Выполняя упражнение, старайтесь держать спину ровно и максимально близко к краю скамьи.\n3. Отжимайтесь так, чтобы плечи находились ниже уровня локтей, а при подъеме были полностью прямые.);");
                db.insert("exercise_store", null, values);

//exercise = 15
                values.put("name", "Приседание плие с гантелей");
                values.put("target_muscle", "Бедра");
                values.put("involved_muscle", "Квадрицепс, Ягодицы");
                values.put("level", 2);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Приседание плие - это базовое упражнение для проработки квадрицепсов, а также нижних пучков ягодичных мышц. Существует несколько вариантов исполнения, как со своим весом, так и со штангой или гантелями.");
                values.put("how_to_do", "1. Исходное положение: установите ноги шире плеч и разверните стопы наружу так, чтобы вы могли держать равновесие. Возьмите гантель обеими руками за ее основание.\n2. На вдохе - медленно выполните приседание, держа голову и спину прямо.\n3. На выдохе - оттолкнитесь пятками от пола, и вернитесь в исходное положение.\n4. Выполните необходимое число повторений.");
                values.put("advice", "1. Приседание плие подходит для людей с хорошо развитой подвижностью в тазобедренных суставах.\n2. Чем ниже вы выполните приседанием, тем лучше. Однако, колени всегда необходимы направлять в сторону носков. Иными словами, делайте приседание до тех пор, пока можете разводить колени в стороны.\n3. Данное упражнение отличается от классических приседаний подбором веса. Выбирайте его на 20 процентов ниже, чтобы вы могли выполнить не менее четырех повторений.");
                db.insert("exercise_store", null, values);

//exercise = 16
                values.put("name", "Тяга гантелей стоя в наклоне");
                values.put("target_muscle", "Широчайшие мышцы спины");
                values.put("involved_muscle", "Бицепс, Плечи");
                values.put("level", 2);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Тяга гантелей в наклоне к поясу позволяет проработать не только широчайшие, но и другие мышцы спины.");
                values.put("how_to_do", "1. Исходное положение: возьмите в руки гантели так, чтобы ладони были направлены вовнутрь. Слегка прогнув спину, сделайте наклон вперед, пока верхняя часть тела не будет практически параллельна полу. Прямые руки опущены перед вами, а колени немного согнуты. Зафиксируйте это положение.\n2. На выдохе - плавно выполните тягу гантелей к талии, сводя лопатки вместе. В верхней точке упражнения сделайте секундную паузу.\n3. На вдохе - медленно вернитесь к исходному положению.\n4. Выполните необходимо число повторов.");
                values.put("advice", "1. Чтобы амплитуда и эффективность упражнения достигли максимума, делайте подъем не только руками, но и лопатками. В верхней точке подъема сведите лопатки друг к другу. В то время как в нижней опустите гантели до уровня колен или чуть ниже. Это позволит добиться наибольшего КПД от упражнения.\n2. Выполняйте упражнение без раскачиваний в разные стороны. Подъем гантелей должен быть только вверх и вниз.\n3. Локти необходимо направить строго вверх без выпячивания в разные стороны. Именно в таком варианте вы проработаете широчайшие мышцы спины.\n4. Положение головы должно быть устремлено вперед, следите в зеркало за своей осанкой.");
                db.insert("exercise_store", null, values);

//exercise = 17
                values.put("name", "Приседание плие со штангой");
                values.put("target_muscle", "Бедра");
                values.put("involved_muscle", "Квадрицепс, Ягодицы");
                values.put("level", 2);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Приседание плие со штангой позволяет проработать внутреннюю часть бедра, а также ягодицы и бицепс бедер. Отличие данного упражнения от приседа сумо в том, что Вы не уводите таз назад, а спину держите прямой на протяжении всего упражнения.");
                values.put("how_to_do", "1. Исходное положение: установите штангу на верхнюю часть трапеции, поставив ноги шире плеч. Разверните носки максимально в стороны, сохраняя при этом равновесие.\n2. На вдохе - медленно выполните присед, до тех пор, пока будро не станет параллельно полу.\n3. На выдохе - отталкиваясь пятками от пола, вернитесь к исходному положению.\n4. Выполните необходимое число приседаний");
                values.put("advice", "1. Всегда контролируйте положение колен во время приседаний - они должны быть всегда направлены в сторону носков.\n2. Каждый подход упражнения можете начинать приседая не в полную амплитуду, каждое следующее повторение делая присед всё глубже.");
                db.insert("exercise_store", null, values);

//exercise = 18
                values.put("name", "Отведение руки с гантелей в наклоне");
                values.put("target_muscle", "Трицепс");
                values.put("involved_muscle", "Плечи, Широчайшие мышцы спины");
                values.put("level", 2);
                values.put("inventory", "Гантели, Силовая скамья");
                values.put("dumbbells", 1); //Гантели
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Это одно из упражнений, направленных на проработку трицепса. Однако его лучше применять как дополнительное, после базовых упражнений для трицепса. Важно отметить то, что выполняя разгибания руки, делайте две паузы в верхней и нижней точке.");
                values.put("how_to_do", "1. Возьмите в одну из рук гантель, обращая ладонь к вашему телу. Немного согните колени и сделайте наклон вперед, сгибаясь в области талии. Взгляд устремлен вперед, а спина практически параллельна уровню пола. Можете опереться другой рукой и коленом в горизонтальную силовую скамью.\n2. Исходное положение: Область руки от локтя до плеча располагается на одном уровне с линией тела. Согните руку в локте под прямым углом, чтобы предплечье было перпендикулярно относительно пола.\n3. На выдохе - плечо находится неподвижным, в то время как, напрягая трицепс, Вы выполняете разгибание руки. В верхней точке зафиксируйте ненадолго руку.\n4. На вдохе - плавно верните руку в исходное положение, снова зафиксировав положение.\n5. После выполнения одной рукой, сделайте упражнение другой.\n6. Выполните необходимое количество повторений.");
                values.put("advice", "1. Положение туловища должно быть строго горизонтальным - бедра и плечи примерно на одном уровне. Это позволит выполнять упражнение с максимальной амплитудой.\n2. Выполняя упражнение, не допускайте раскачивания корпуса Вашего тела. Помните, что Вы должны заблокировать все части своего тела и только предплечье двигается усилием трицепса.\n3. Не позволяйте руке гулять словно качели, так как это сильно снизит эффективность упражнения.\n4. Подбирайте вес гантелей таким образом, чтобы вы сделали не менее десяти повторений.");
                db.insert("exercise_store", null, values);

//exercise = 19
                values.put("name", "Подъем ног в висе");
                values.put("target_muscle", "Пресс");
                values.put("involved_muscle", "Нижняя часть спины");
                values.put("level", 1);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Подъем ног в висе на перекладине является одним из самых лучших упражнений для проработки мышц пресса. Подъем можно осуществлять как с согнутыми ногами, так и с прямыми.");
                values.put("how_to_do", "1. Исходное положение: возьмитесь узким хватом и повисните на перекладине.\n2. На выдохе - сделайте подъем ног вверх, пока не коснетесь коленями груди.\n3. На вдохе - медленно опустите ноги в исходное положение.\n4. Сделайте подъем ног требуемое число повторов.");
                values.put("advice", "1. При неправильном исполнении упражнения амплитуда уменьшается, а ноги находятся на уровне горизонтали. В таком исполнении работает только часть живота и эффективность уменьшается.\n2. Выполняя упражнение, не помогайте себе руками и не раскачивайтесь.\n3. Ноги можно не опускать до нижней точки. Это позволит сильнее нагрузить мышцы пресса.\n4. Подъем ног можно делать не только прямо к груди, но и разные стороны. Так Вы сможете включить косые мышцы живота.");
                db.insert("exercise_store", null, values);

//exercise = 20
                values.put("name", "Жим штанги лежа на наклонной скамье");
                values.put("target_muscle", "Грудь");
                values.put("involved_muscle", "Плечи, Трицепс");
                values.put("level", 1);
                values.put("inventory", "Штанга, Силовая скамья");
                values.put("barbell", 1); //Штанга
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Жим штанги, лежа на наклонной скамье, принципиально отличается от классического жима штанги, где жим выполняется на горизонтальной скамье. Отличие заключается в том, что мышцы груди прорабатываются более качественно и эффективно чем в горизонтальном жиме.");
                values.put("how_to_do", "1. Выберите угол наклона скамьи (оптимальный 30 градусов). Лягте на наклонную скамью, закрепив ноги за опорные валики.\n2. Исходное положение: возьмите гриф хватом немного уже, чем в классическом жиме и поднимите его над грудью. Это позволит увеличить амплитуду движения и сокращение грудных мышц.\n3. На вдохе - плавно опускайте штангу вниз до тех пор, пока гриф не коснется груди.\n4. На выдохе - сделайте небольшую паузу и поднимите штангу в исходное положение.\n5. Сделайте жим штанги требуемое количество раз.");
                values.put("advice", "1. Если Вы делаете жим штанги на наклонной скамье впервые, то попросите напарника вас подстраховать.\n2. Необязательно держать штангу на уровне Ваших глаз. Достаточно просто поднимать ее вертикально вверх от груди.\n3. Наиболее оптимальный угол наклона скамьи 30 градусов.\n4. Желательно использовать кистевые бинты, для безопасности Ваших запястий.\n5. Поскольку это базовое упражнение, вы можете делать его в вместо классического жима. В этом случае результат, достигнутый в горизонтальном жиме не упадет.");
                db.insert("exercise_store", null, values);

//exercise = 21
                values.put("name", "Жим гантелей стоя");
                values.put("target_muscle", "Плечи");
                values.put("involved_muscle", "Трицепс");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Жим гантелей стоя относится к базовому типу упражнений. Основная нагрузка направленна переднюю часть дельт. Преимущество жима гантелей в том, что их можно разворачивать в верхней и нижней точке упражнения для большей эффективности");
                values.put("how_to_do", "1. Исходное положение - поставьте ноги на ширине плеч и возьмите в руки гантели. Держите спину ровно. Поднимите гантели до уровня глаз, согнув локти под углом в 90 градусов.\n2. На выдохе - сделайте жим гантелей над головой, немного сводя, их друг к другу.\n3. На вдохе - возвратитесь к исходному положению.\n4. Сделайте необходимое количество повторов.");
                values.put("advice", "1. Задержку нужно делать только в верхней точке упражнения. Если вы сделаете паузу внизу, то эффективность упражнения будет стремиться к нулю.\n2. Опускать гантели необходимо так, чтобы они располагались на уровне ваших плеч. Знайте, что чем больше амплитуда, тем выше эффективность упражнения.\n3. Жим с попеременной сменой рук будет отличным вариантом для начинающих спортсменов. Это позволит упростить технику выполнения.");
                db.insert("exercise_store", null, values);

//exercise = 22
                values.put("name", "Сгибание рук с гантелями стоя");
                values.put("target_muscle", "Бицепс");
                values.put("involved_muscle", "Предплечья");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Сгибание рук с гантелями стоя является одним из наилучших упражнений для развития бицепса. Превосходство гантели над штангой заключается в том, что вы сможете выполнять упражнение с вращением кисти наружу - супинацией. Это позволит включить плечевую и плечелучевую мышцу.");
                values.put("how_to_do", "1. Исходное положение: держа гантели в руках, встаньте прямо и опустите руки вдоль тела, прижав локти к туловищу. Ладони направлены вперед.\n2. На выдохе - не отрывая локти от тела, сделайте подъем рук с гантелями до уровня ваших плеч. В верхней точке упражнения немного остановитесь, напрягая бицепс.\n3. На вдохе - медленно разогните руки и возвратитесь к исходному положению.\n4. Выполните сгибание рук с гантелями требуемое количество раз.");
                values.put("advice", "1. Подбирайте вес гантелей таким образом, чтобы он не был причиной нарушения техники упражнения.\n2. Выполнение этого упражнение с попеременной сменой рукой позволяет использовать гантели с большей тяжестью.\n3. Не поднимайте гантели выше требуемого уровня, так как это снижает нагрузку с бицепса и уменьшает эффективность упражнения.\n4. Если делать упражнение не одновременно двумя руками, а попеременно, то можно поднять немного больший вес, однако, следите за тем, чтобы Ваш корпус не раскачивался.");
                db.insert("exercise_store", null, values);

//exercise = 23
                values.put("name", "Выпады со штангой");
                values.put("target_muscle", "Ягодицы");
                values.put("involved_muscle", "Квадрицепс");
                values.put("level", 1);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Выпады со штангой это отличное упражнение для тренировки большой ягодичной мышцы и квадрицепсов. Самый распространенный вариант - выпады со штангой. Если вы мечтаете о красивых ягодицах, то это упражнение для Вас.");
                values.put("how_to_do", "1. Зафиксируйте гриф на стойках чуть ниже ваших плеч. Снарядите требуемой тяжестью. Подсаживаемся под штангу и фиксируем гриф на плечах руками. Напрягая тело и мышцы ног, приподнимаем гриф и отходим от стойки.\n2. Исходное положение: стойте прямо, ноги на ширине плеч.\n3. На вдохе - выполняйте шаг вперед одной из ног: следите за равновесием и плавно сделайте выпад со штангой. Выбирайте такую длину шага, чтобы в нижней точке в коленном суставе образовывался прямой угол. Старайтесь держать спину прямой, а голову приподнятой.\n4. На выдохе - При помощи толчка ступни от пола поднимитесь в исходное положение.\n5. Выполните требуемое количество повторений для каждой из ног.");
                values.put("advice", "1. Если вы хотите усложнить упражнение и повысить его эффективность, то поставьте переднюю ногу на небольшой подъем до 20 см. Это позволит Вам делать выпад ниже и лучше растягивать ягодичную мышцу.\n2. Чтобы лучше держать равновесие установите ноги не на одной прямой, а разводя их немного в стороны.\n3. Если выполняя это упражнение вы испытываете сложности с удержанием равновесия, то попытайтесь сделать его без нагрузки, используя исключительно свой вес.\n4. Возвращаясь в исходное положение, старайтесь толчок осуществлять пяткой.\n5. Есть вариант выполнения выпадов, в котором исходное положение с выставленной впереди ногой. И в таком варианте исполнения тело двигается вверх и вниз усилием мышц впереди стоящей ноги.");
                db.insert("exercise_store", null, values);

//exercise = 24
                values.put("name", "Подтягивания прямым хватом");
                values.put("target_muscle", "Широчайшие мышцы спины");
                values.put("involved_muscle", "Бицепс, Грудь");
                values.put("level", 1);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Подтягивания прямым классическим - прямым хватом является одним из базовых упражнений, которое должен освоить каждый спортсмен. Это упражнение направлено на прокачивание широчайших мышц спины, бицепса, а также мышц грудного отдела.");
                values.put("how_to_do", "1. Исходное положение: выполните хват перекладины на уровне ширины плеч прямым хватом (ладонями от себя так, чтобы большой палец охватывал ее снизу, закрывая ее в замок). Ноги держите вместе.\n2. На выдохе - сделайте подтягивание до того момента, пока перекладина не достигнет верхнего уровня груди.\n3. На вдохе - полностью распрямляя руки, медленно опуститесь к исходному положению.\n4. Сделайте требуемое количество повторений.");
                values.put("advice", "1. Если вы с трудом выполняете подтягивания, то попросите кого-либо помогать вам, подталкивая руками за ноги. Это позволит сделать большее количество повторений. Или используйте гравитрон.\n2. Есть мнение, что для лучшего эффекта необходимо поднимать тело вверх в 2 раза быстрее, чем опускать его в исходное положение.\n3. Выполняйте упражнение плавно и без раскачки, используя лишь мышцы рук и спины.");
                db.insert("exercise_store", null, values);

//exercise = 25
                values.put("name", "Тяга гантелей лежа на скамье на животе");
                values.put("target_muscle", "Широчайшие мышцы спины");
                values.put("involved_muscle", "Плечи");
                values.put("level", 1);
                values.put("inventory", "Гантели, Силовая скамья");
                values.put("dumbbells", 1); //Гантели
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Это упражнение для тренировки широчайших мышц спины и заднего пучка дельтовидных мышц. Основное преимущество упражнения в том, что грудь упирается в скамью, тем самым снимая нагрузку с поясницы.");
                values.put("how_to_do", "1. Исходное положение: нейтральным хватом возьмите гантели и лягте на скамью животом вниз. Руки необходимо опустить вниз.\n2. На выдохе - сгибая руки в локтях, выполните подъем гантелей вверх до уровня груди. Находясь в этом положении, сделайте небольшую задержку.\n3. На вдохе - медленно опустите гантели к исходному положению.\n4. Выполните необходимое количество повторений.");
                values.put("advice", "1. Выполняя упражнение, старайтесь не разводить локти в сторону, так как это сместит нагрузку на другие мышцы и уменьшит эффективность.\n2. Не поднимайте плечи вверх, а сводите лопатки вместе, так лучше будут работать мышцы спины");
                db.insert("exercise_store", null, values);

//exercise = 26
                values.put("name", "Скручивания на верхнем блоке");
                values.put("target_muscle", "Пресс");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "Оличное упражнение на пресс. Отличительная особенность - пресс находится в постоянном напряжении");
                values.put("how_to_do", "1. Возьмитесь двумя руками за ручки троса, закрепленного на верхнем блоке.\n2. Исходное положение: встаньте на колени не дальше полметра от блока, держите руки у лба. Слегка наклонитесь вперед.\n3. На выдохе, усилием мышц пресса, скручивать туловище. Старайтесь тянуть нос к низу живота.\n4. На вдохе медленно вернитесь в исходное положение.\n5. Выполните необходимое количество повторений.");
                values.put("advice", "1. Выполняйте скручивание туловища, а не наклоны вниз. Задача именно скрутить тело.\n2. Совет: во время выполнения упражнения держите мышцы пресса в постоянном напряжении. Также не устанавливайте слишком тяжелый вес, так как в этом случае основная работа придется на нижнюю часть спины.\n3. Основная фишка скручиваний на вертикальном блоке в том, что пресс постоянно находится в напряженном состоянии - потому что трос не дает выпрямляться полностью.\n4. Обычно это упражнение делается с канатными ручками, но можно делать и с твёрдой ручкой - тогда ручку надо держать за головой. Можно также выполнять не на коленях, а сидя на скамье.\n5. Для усиления эффекта в нижней точке можно задерживаться на секунду. А выпрямляться можно не до конца.");
                db.insert("exercise_store", null, values);

//exercise = 27
                values.put("name", "Подъем на носки стоя");
                values.put("target_muscle", "Икры");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Гантели, Штанга, Машина Смитта");
                values.put("dumbbells_barbell", 1); //Гантели-Штанга
                values.put("description", "Несложное упражнение для развития икр. Выполняется со штангой, но можно использовать гантели или выполнять упражнение в машине Смитта.");
                values.put("how_to_do", "1. Штанга находится на стайках. возьмите штангу средним закрытым хватом. Подсядьте под гриф, сведите лопатки расположите гриф на трапециевидных мышцах. Снимите штангу со стоек\n2. Исходное положение: установите носки на возвышенность (блины от штанги), ноги на ширине плеч.\n3. На выдохе - плавно поднимайтесь на носки по максимальной амплитуде. Спину держите ровно. В верхней точке фиксация\n4. На вдохе - медленно вернитесь в исходное положение, стараясь коснуться пятками пола как можно плавнее\n5. Выполните подъем на носки нужное число повторений.");
                values.put("advice", "1. Если у Вас есть проблемы с поясницей, то используйте пояс и выполняйте подъемы в машине Смитта\n2. Старайтесь подбирать такой вес, чтобы Вы могли выполнить 20-25 повторений в подходе");
                db.insert("exercise_store", null, values);

//exercise = 28
                values.put("name", "Шаги со штангой стоя");
                values.put("target_muscle", "Трапеции");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Самое популярное упражнение для тренировки трапеции. Обязательно используйте пояс для снижения нагрузки на позвоночник. Можете использовать лямки для снижения нагрузок на кисти.");
                values.put("how_to_do", "1. Возьмите штангу хватом сверху на ширине плеч.\n2. Исходное положение: ноги на ширине плеч, немного согнуты в коленях. Спина ровная, прогнута.\n3. На вдохе - усилием трапеций поднимите штангу вверх. Задержитесь в верхней точке.\n4. На выдохе - вернитесь в исходное положение.");
                values.put("advice", "1. В верхней точке попробуйте на секунду задержаться, старайтесь вниз движение делать медленнее.\n2. Делать это упражнение рекомендуется до жжения в трапециях. Вес берите такой, чтобы сделать хотя бы 8 повторений.\n3. Если вы новичек, то не старайтесь заниматься читингом на этом упражнении.\n4. Делайте шраги в день тренировки плеч. Т.к. трапеции будут уже косвенно нагружены и подготовленны к нагрузке.\n5. Не делайте круговых движений - только вверх и вниз.");
                db.insert("exercise_store", null, values);

//exercise = 29
                values.put("name", "Болгарские выпады");
                values.put("target_muscle", "Квадрицепс");
                values.put("involved_muscle", "Ягодицы, Бедра, Икры");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Болгарские выпады отличное упражнение для того чтобы сделать свои ягодицы округлыми, а бедра подтянутые. Часто болгарские выпады по эффективности сравнивают:");
                values.put("how_to_do", "1. Исходное положение: с гантелями в руках подойдите к невысокой скамье. Закиньте подъем одной ноги на край скамья, второй сделайте небольшой шаг вперед. Спина прямая, руки свободно опущены вниз, смотрите прямо.\n2. На вдохе - опускайтесь вниз, сгибая в колене опорную ногу. Продолжайте движение пока бедро не станет параллельно полу.\n3. На выдохе - усилием мышц ноги (опираясь на пятку) вернитесь в исходное положение.\n4. После выполнения болгарских выпадов необходимое количество раз на одной ноге, повторите упражнение с опорой на другую ногу.");
                values.put("advice", "1. Носок опорной ноги немного заворачивайте вовнутрь\n2. Ногу ставьте с таким удалением, чтобы в нижней точке упражнения носок был на одной линии с коленом\n3. Старайтесь при подъеме опираться на пятку\n4. Голову держите прямо, не наклоняйтесь, стараясь смотреть под ноги\n5. Выполнять болгарские выпады следует медленно и подконтрольно\n6. Чем выше опора, тем выпад получается глубже, следовательно, нагрузка больше");
                db.insert("exercise_store", null, values);

//exercise = 30
                values.put("name", "Подъем таза одной ногой");
                values.put("target_muscle", "Ягодицы");
                values.put("involved_muscle", "Бедра");
                values.put("level", 1);
                values.put("inventory", "Отсутствует");
                values.put("description", "Упражнение не очень распространенное, но тем не менее эффективное. Отлично дополнит другие базовые упражнения на ягодицы (");
                values.put("how_to_do", "1. Исходное положение: лягте на спину, согните ноги в коленях под прямым углом. Руки на поясе, или расставлены в стороны.\n2. Выпрямите одну ногу.\n3. На выдохе - поднимите таз как можно выше, удерживая прямую ногу неподвижной.\n4. На вдохе - вернитесь в исходное положение, слегка коснувшись тазом пола.\n5. Выполните необходимое количество повторений одной ногой, затем поменяйте ногу и сделайте упражнение с упором на другую ногу.");
                values.put("advice", "1. Может выполнятся как на полу, так и на скамье. Вариант со скамьей позволяет выполнять упражнение с большей амплитудой. При выполнении с использованием скамьи необходимо лопатками лечь на нее (руки можно держать прямыми, положив их на скамью).\n2. Отталкивайтесь пяткой, только так мышцы ягодицы будут работать с максимальным эффектом. Даже можно поставить стопу на пятку.\n3. Для усиления нагрузки можете задерживаться в верхней точке на несколько секунд.");
                db.insert("exercise_store", null, values);

//exercise = 31
                values.put("name", "Зашагивания на скамью");
                values.put("target_muscle", "Бедра");
                values.put("involved_muscle", "Ягодицы");
                values.put("level", 2);
                values.put("inventory", "Гантели, Штанга");
                values.put("dumbbells_barbell", 1); //Гантели-Штанга
                values.put("description", "Все знают, что королем всех упражнений считаются приседания, однако, есть множество других эффективных упражнений для тренировки ног. Одним из таких упражнений является зашагивания на скамью - это упражнение можно использовать как в дополнение к приседаниям, так и взамен их - с целью разнообразить и освежить тренинг.\nВыполняются зашагивания со штангй или с гантелями.\nУпражнение хорошо подходит для девушек и успешно выполняется в домашних условиях.");
                values.put("how_to_do", "1. Исходное положение - встаньте с утяжелением (гантели или штанга) перед скамьей или платформой.\n2. Поставьте правую ногу на скамью.\n3. На выдохе - опираясь на пятку правой ноги и переместив на нее центр тяжести, поднимите тело и поставьте левую ногу на скамью\n4. На вдохе - медленно опустите левую ногу на пол.\n5. Повторите защагивания с опорой на правую ногу необходимое число раз, затем сделайте упражнение с опорой на левую ногу.");
                values.put("advice", "1. Зашагивания делаются обычно в 2х вариантах: полные - вторая нога ставится на лавку рядом с опорной, и неполные - вторая нога остаётся в воздухе. Делая полные зашагивания нагрузка на мышцы меньше, т.к. опорная нога разгружается. Неполные зашагивания делать труднее - нагрузка практически постоянно сосредоточена на рабочей ноге.\n2. Вначале выполняйте зашагивать одной ногой нужное количество раз, и только потом другой. Не стоит чередовать ноги каждый раз - так они загрузятся менее эффективно.\n3. Обязательно сосредотачивайте усилие на пятке - только так мышцы будут работать правильно.\n4. Если гантели тяжелые, то используйте лямки, чтобы не травмировать кисти.\n5. Старайтесь подбирать высоту ступеньки такой, чтобы колено при зашагивании было на уровне тазобедренного сустава.");
                db.insert("exercise_store", null, values);

//exercise = 32
                values.put("name", "Подъем ягодиц со штангой на полу");
                values.put("target_muscle", "Ягодицы");
                values.put("involved_muscle", "Бедра");
                values.put("level", 1);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Подъем ягодиц со штангой на полу - отличное упражнение для ягодиц. Большим его преимуществом является возможность его выполнения в домашних условиях. Упражнение аналогично");
                values.put("how_to_do", "1. Исходное положение: сядьте на пол, ноги расположите под нагруженной штангой. Лягте на пол, гриф расположите на бедрах.\n2. На выдохе - упираясь пятками в пол поднимайте штангу вверх усилием ягодиц (руками можете придерживать гриф). Пытайтесь поднять таз максимально высоко.\n3. На вдохе - вернитесь в исходное положение.\n4. Выполните подъем ягодиц со штангой необходимое количество раз.");
                values.put("advice", "1. Во время выполнения подъема ягодиц ставьте ноги поближе к корпусу, так Вы сильнее нагрузите ягодичную мышцу.\n2. Отягощение удерживайте двумя руками и располагайте его на верхней части бедер. Если вес отягощения очень большой, подкладывайте под него что-нибудь.");
                db.insert("exercise_store", null, values);

//exercise = 33
                values.put("name", "Отведение ноги в сторону в кроссовере");
                values.put("target_muscle", "Ягодицы");
                values.put("involved_muscle", "Бедра");
                values.put("level", 1);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "Отведение ноги в сторону в кроссовере на нижнем блоке - упражнение, любимое многими женщинами, но не все делают его с необходимой нагрузкой - халтурят. Правильно выполнять это упражнение медленно, с паузой в крайней точке. Как основное данное упражнение делать не рекомендуется, но зато оно прекрасно подойдет как вспомогательное упражнение для точной проработки мышц. Выполняется данное упражнение на нижнем блоке кроссовера или на специальном тренажере.");
                values.put("how_to_do", "1. Исходное положение: установите необходимый вес. Если используете кроссовер, то прикрепите к тросу специальный браслет с застежкой и застегните его на лодыжке. Встаньте свободным боком к кроссоверу, рукой держитесь за раму тренажера.\n2. На выдохе - держа ногу выпрямленной, отведите её в сторону как можно дальше, и в крайней точке сделайте паузу.\n3. На вдохе - верните нагруженную ногу к опорной ноге.\n4. Сделав необходимое число отведений на одну ногу, повернитесь другим боком и сделайте тоже самое для другой ноги.");
                values.put("advice", "1. Еще лучше заводить рабочую ногу внахлест к ней так лучше сокращаются мышцы.\n2. Не гонитесь за большими весами - сконцентрируйтесь на технике выполнения упражнения.\n3. Выбирайте такое упражнение у кроссовера, при котором вы будете отводить ногу строго в сторону относительно Вашего тела.\n4. Не сгибайте рабочую ногу. Хоть это и облегчает намного выполнение упражнения, но и включает в работу целую группу других мышц.\n5. Не отклоняйтесь, держите корпус прямо - ягодичная мышца сокращается тем сильнее, чем меньше угол между ногой и корпусом.\n6. Работайте в полную амплитуду - чем большее расстояние пройдет нога при выполнении отведения, тем лучше для Вашей попы. И не забывайте про паузу в верхней точке.");
                db.insert("exercise_store", null, values);

//exercise = 34
                values.put("name", "Подъем ягодиц со штангой опираясь на скамью");
                values.put("target_muscle", "Ягодицы");
                values.put("involved_muscle", "Бедра");
                values.put("level", 1);
                values.put("inventory", "Штанга, Силовая скамья");
                values.put("barbell", 1); //Штанга
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Подъем ягодиц со штангой (ягодичный мостик) с опорой на скамью - хорошее упражнение, прорабатывающее ваши ягодичные мышцы. Возможны варианты где в качестве утяжеления используются блины от штанги. Всем девушкам, которые хотят иметь подтянутую попу - обязательно выполнять! Кроме ягодиц хорошо работают в этом упражнении бедра и икры. Подъем ягодиц подойдет и в начале тренировки для разминки и в конце - \"на добивание\". За счет того, что в упражнении используется скамья, таз мажет опускаться больше, за счет чего эффективность упражнения возрастает.");
                values.put("how_to_do", "1. Исходное положение: сядьте перед скамьей, ноги расположите под нагруженной штангой. Лопатками упритесь в скамью.\n2. На выдохе - бедрами начните поднимать штангу вверх (руками можете придерживать гриф). Поднимайте таз как можно выше\n3. На вдохе - вернитесь в исходное положение.\n4. Выполните подъем ягодиц со штангой необходимое количество раз.");
                values.put("advice", "1. В верхней точке - в момент наибольшего напряжения задерживайтесь на секунду.\n2. При использовании штанги с большим весом подкладывайте под нее полотенце - чтобы во время упражнения не было болевых ощущений.");
                db.insert("exercise_store", null, values);

//exercise = 35
                values.put("name", "Сгибание рук на нижнем блоке в кроссовере");
                values.put("target_muscle", "Бицепс");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "Для развития бицепса помимо упражнений со штангой и гантелями эффективно используются блочные тренажеры. Конечно не стоит заниматься только на тросовых тренажерах - работа со свободными весами должна быть основой любой тренировки, но сочетание с тросовым тренажером даст отличные результаты, обязательно разнообразие в упражнениях!");
                values.put("how_to_do", "1. Исходное положение: возьмитесь за рукоятку, прикрепленную к тросу нижнего блока, нижним хватом. Встаньте прямо, слегка отклонитесь назад. Локти прижмите к корпусу. Ноги на ширине плеч.\n2. На выдохе - сгибайте предплечья до полного сокращения бицепса. Следите, чтобы локти не уходили вперед, а корпус не раскачивался.\n3. На вдохе - вернитесь в исходное положение.\n4. Выполните сгибание рук необходимое число раз.");
                values.put("advice", "1. Чем дальше вы расположитесь от блока, тем сложнее будет выполнять упражнение - за счет того, что трос тянет не вниз, а в сторону блока, бицепс будет постоянно находится в напряжении.\n2. Делая сгибание рук со штангой, вы меньше задействуете бицепсы в нижней и верхней фазе упражнения. В сгибании рук на нижнем блоке нагрузка будет на всех фазах упражнения.\n3. Используя различные рукоятки можно перемещать нагрузку с наружней головки бицепса (прямая рукоять) на внутреннюю (изогнутая).\n4. Держите поясницу во время выполнения упражнения прямой - для правильного положения смотрите вперед, и немного приподнимите подбородок.");
                db.insert("exercise_store", null, values);

//exercise = 36
                values.put("name", "Подъем гантелей перед собой");
                values.put("target_muscle", "Плечи");
                values.put("involved_muscle", "Грудь");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Подъем гантелей перед собой - одно из самых доступных упражнений для тренировки передней части плечей. Для выполнения нужны только гантели - поэтому упражнение можно делать в домашних условиях. Отлично подойдет для увеличения передних дельт. Может выполняться с двумя гантелями (поднимаются как поочередно, так и одновременно), с одной гантелей, с блином от штанги, со штангой и в блочном тренажере.");
                values.put("how_to_do", "1. Исходное положение: возьмите гантели хватом сверху, ладони поверните друг к другу, встаньте прямо.\n2. На выдохе - поднимите руки перед грудью усилием передней части плеча - до уровня подбородка или немного выше. Положение локтя зафиксируйте - не сгибайте его во время движения.\n3. На вдохе - вернитесь в исходное положение.\n4. Выполните подъем нужное число повторений.");
                values.put("advice", "1. Если хотите задействовать передние дельты максимально, берите одну гантель с большим весом и поднимайте её на 45 градусов выше лини плеч. Выше поднимать нет смысл - т.к. нагрузка смещается на трапециевидные мышцы.\n2. Туловище во время подъема гантелей держите неподвижным, если не можете поднимать без читинга, то уменьшите вес гантелей. Читинг - только для опытных спортсменов.\n3. Во время упражнения руку в локте немного сгибайте.");
                db.insert("exercise_store", null, values);

//exercise = 37
                values.put("name", "Жим штанги головой вниз");
                values.put("target_muscle", "Грудь");
                values.put("involved_muscle", "Плечи, Трапеции");
                values.put("level", 1);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Упражнение по задействованным мышцам аналогично жиму штанги лежа. Отличие - основная нагрузка приходиться на нижнюю часть груди.");
                values.put("how_to_do", "1. Отрегулируйте наклон скамьи под нужным углом. Закрепите ноги за опорные валики.\n2. Исходное положение - прямой хват, руки на ширине плеч, поднимите штангу со стоек. Удерживайте гриф штанги прямо над низом грудных мышц. Руки перпендикулярны полу.\n3. На вдохе - медленно опускайте штангу вниз. Продолжайте опускать до тех пор, пока штанга не коснется груди.\n4. На выдохе - верните штангу в исходное положение.\n5. Повторите жим штанги необходимое количество раз.");
                values.put("advice", "1. если Вы недавно занимаетесь в тренажерном зале, то обязательно просите подстраховать, как вариант, выполняйте упражнение в машине Смита;\n2. старайтесь, чтобы штанга двигалась только в вертикальной траектории (вверх-вниз);\n3. не допускайте удара штангой в нижней точке (отбива);\n4. следите за своими локтями, они должны быть всегда направлены в стороны.</ul>");
                db.insert("exercise_store", null, values);

//exercise = 38
                values.put("name", "Сгибание ног в тренажёре сидя");
                values.put("target_muscle", "Бедра");
                values.put("involved_muscle", "Икры");
                values.put("level", 1);
                values.put("inventory", "Рычажные тренажеры");
                values.put("lever_trainers", 1); //Рычажные тренажеры
                values.put("description", "Упражнение-альтернатива сгибанию ног в тренажере лежа. Не так популярно, ввиду того, что тренажер менее распространен и не так удобен. Рекомендуется чередовать упражнения в сидячем и лежачем тренажерах, т.к. они по-разному влияют на сухожилия.");
                values.put("how_to_do", "1. Установите необходимый вес в тренажере.\n2. Исходное положение - сядьте в тренажер, прижмите поясницу к спинке тренажера. Ступнями ног зацепитесь за валик, установите их параллельно друг-другу. Руками держитесь за ручки тренажера.\n3. На выдохе - согните ноги в коленях усилием мышц бедра.\n4. На вдохе - не расслабляя мышц задней поверхности бедра, вернитесь в исходное положение.\n5. Выполните сгибание ног необходимое количество раз.");
                values.put("advice", "1. Отрегулируйте тренажер под себя - верхний валик должен давить на бедра возле коленей, нижний - в щиколотку рядом с пяткой.\n2. Сгибайте ноги как можно сильнее, а разгибать полностью не стоит.\n3. Необходимо следить за техникой, чтобы избежать излишней нагрузки на коленные суставы - избегать рывков и импульсной нагрузки.\n4. Это упражнение - изолирующее. Делайте его во второй половине тренировки - чтобы добить мышцы.");
                db.insert("exercise_store", null, values);

//exercise = 39
                values.put("name", "Разведение гантелей стоя в наклоне");
                values.put("target_muscle", "Плечи");
                values.put("involved_muscle", "Трапеции");
                values.put("level", 2);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Обычно задняя дельтовидная мышца является самой отстающей частью мышц плеча. Это упражнение отлично подойдет для развития задних, а также средних дельт.");
                values.put("how_to_do", "1. Исходное положение - наклонитесь вперед практически параллельно полу. Можете даже упереться лбом в подходящую опору (подойдет угол скамьи). Руки опущены вниз, слегка согнуты в локтях, ладони обращены друг к другу.\n2. На выдохе - оставляя корпус тела неподвижным, разводите гантели в стороны, сохраняя небольшой изгиб в локтевых суставах. Движение выполняйте немного не доводя руки до параллели к полу.\n3. На вдохе - верните руки с гантелями по такой же дуге.\n4. Выполните разведение гантелей необходимое число повторов.");
                values.put("advice", "1. Не гонитесь за весом, сосредоточьтесь на технике упражнения и неподвижности корпуса тела.\n2. Упражнение требует немалого усилия мышц-стабилизаторов, поэтому, сначала попробуйте упираться лбом или вообще делать упражнение лежа на силовой скамье.\n3. При выполнении упражнения локти и плечи должны находится в одной плоскости. Необходимо следить за тем, чтобы локти всегда были согнуты в локтях (около 20 градусов), так нагрузка ложится на целевые мышцы.</ul>");
                db.insert("exercise_store", null, values);

//exercise = 40
                values.put("name", "Тяга т-грифа в наклоне");
                values.put("target_muscle", "Широчайшие мышцы спины");
                values.put("involved_muscle", "Бицепс, Плечи");
                values.put("level", 2);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Отличное упражнение для нагрузки мышц спины. Является аналогом тяги штанги в наклоне, но за счет того, что снаряд не гуляет в стороны, выполнять тягу т-грифа проще. Это позволяет взять больший вес, чем в тяге штанги в наклоне. Упражнение выполняется как в специальном тренажёре, так и со штангой (в этом случае рекомендуется упереть штангу в угол).");
                values.put("how_to_do", "1. Исходное положение - наклонитесь вперед практически параллельно полу. Слегка согните колени. Спину держите прямо. Возьмите рукоять тренажера. Если выполняете с грифом, то возьмите гриф сразу под дисками.\n2. На выдохе - тяните штангу на себя усилием мышц спины.\n3. На вдохе - верните штангу в исходное положение.\n4. Выполните тягу необходимое число повторов.");
                values.put("advice", "1. Если выполняете упражнение на тренажере, то чем уже хват, тем сильнее в работу включены бицепсы, чем шире хват, тем меньше амплитуда, и тяга т-грифа становится менее эффективной.\n2. В нижней точке упражнения не разгибайте руки до конца, за счет этого вы не будете переносить нагрузку со спины на руки.\n3. Самые распространенные ошибки: сгорбленная спина и слишком малый наклон туловища. Корпуса должен быть практически параллелен полу - 70 - 80 градусов.\n4. Упражнение на тренажере выполняется как прямым, так и обратным хватом - как кому нравиться.\n5. В упражнении двигаются не только руки, но и лопатки. В верхней точке сводите лопатки вместе, расправляя грудь - так мышцы спины сократятся сильнее.");
                db.insert("exercise_store", null, values);

//exercise = 41
                values.put("name", "Разведение рук назад в кроссовере");
                values.put("target_muscle", "Плечи");
                values.put("involved_muscle", "Трапеции");
                values.put("level", 2);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "При тренировке заднего пучка дельтовидных мышц стоит обратить внимание на особенность - её непросто изолировать от других мышцы. Обычно в упражнениях на заднюю дельту активно задействуются трицепсы и широчайшие мышцы спины. Работая в кроссовере, можно добиться максимальной загрузки задней дельты.");
                values.put("how_to_do", "1. Установите необходимый вес в блоках кроссовера, установите удобные ручки на тросы и встаньте по центру. Возьмите ручки прямым хватом, таким образом, чтобы в правой руке была левая ручка, а в левой руке - правая.\n2. Исходное положение - поставьте одну ногу вперед, а вторую согните. Положение должно быть устойчивым. Сведите перед собой прямые руки на уровне глаз.\n3. На выдохе - усилием дельтовидных мышц, отведите руки назад, при этом траектория должна быть на уровне глаз.\n4. На вдохе - верните руки в исходное положение.\n5. Сделайте необходимое количество повторений.");
                values.put("advice", "1. Руки всегда должны быть немного согнуты в локтях. Полностью выпрямляя руки в конце движения вы задействуете трицепс.\n2. Самая распространенная ошибка - отведение головы назад и сведение лопаток вместе. Так в работу включается верхняя часть спины.\n3. Сводя руки вместе вы сильнее растягиваете заднюю дельту. Можно заводить руку одну под другую, но тогда рекомендуем чередовать руки на каждом повторении.\n4. А разводить руки в стороны полностью не стоит - так в работу включается спина.");
                db.insert("exercise_store", null, values);

//exercise = 42
                values.put("name", "Разгибание ног в тренажере");
                values.put("target_muscle", "Квадрицепс");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Рычажные тренажеры");
                values.put("lever_trainers", 1); //Рычажные тренажеры
                values.put("description", "Разгибание ног в тренажере - изолирующее для мышц квадрицепса. Популярное упражнение для тренировки мышц ног. Не гонитесь за большим весом, для того, чтобы не нанести травму коленям.");
                values.put("how_to_do", "1. Выставьте на тренажере необходимый вес. Отрегулируйте рычаг тренажера для разгибания ног так, чтобы валик находился прямо над ступнями. Сядьте в тренажер\n2. Исходное положение - зафиксируйте в тренажере ноги. Руками возьмитесь за рукоятки. Колени должны быть согнуты под прямым углом.\n3. На выдохе - усилием мышц бедер, выпрямите ноги. Следите, чтобы верхняя часть тела была неподвижна. Задержитесь в этом положении на несколько секунд.\n4. На вдохе - верните ноги в исходное положение.\n5. Выполните разгибание ног нужное количество повторений.");
                values.put("advice", "1. Разгибание ног в тренажере не сделает Ваши ноги сильнее - он направлено на то, чтобы увеличить объем мышц. Делайте это упражнение в дополнение к базовым.\n2. Повернув носки друг к другу, сильнее нагрузите внутреннюю головку четырехглавой. Если в стороны, то наружнюю.\n3. Разгибание ног отлично сочетается в суперсетах с базовыми упражнениями для ног. Например, с приседаниями.");
                db.insert("exercise_store", null, values);

//exercise = 43
                values.put("name", "Сгибание рук с гантелями хватом «молот»");
                values.put("target_muscle", "Бицепс");
                values.put("involved_muscle", "Предплечья");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Держа гантели в руке таким образом, нагрузка ложится на внутреннюю головку бицепса и предплечье. Простое и эффективное изолирующее упражнение. Варианты выполнения различные - можете выполнять как одновременно, так и попеременно. Начинающим культуристам рекомендуется делать попеременно, чтобы не допускать раскачиваний. Молот лучше делать в конце тренировки на руки.");
                values.put("how_to_do", "1. Возьмите гантели с нужным весом в каждую руку. Встаньте прямо.\n2. Исходное положение: руки опущены вниз, ладони внутрь, локти прижаты к туловищу, спина прямая.\n3. На выдохе - сгибайте руку в локте, усилием бицепса поднимая гантели. Поднимайте гантель максимально высоко, до полного сокращения бицепса (примерно на уровне плеча). Руки от плеча до локтя должны быть неподвижны.\n4. На вдохе - верните гантели в исходное положение.\n5. Повторите сгибание рук необходимое число раз.");
                values.put("advice", "1. Не двигайте локти и не раскачивайтесь корпусом - так вы будете поднимаете гантели не силой мышц, а за счет инерции - это снижает эффективность упражнения.\n2. Если делаете упражнение молоток сидя на силовой скамье, то опирайтесь на спинку (под углом 90 градусов) - так вы не будете раскачиваться.");
                db.insert("exercise_store", null, values);

//exercise = 44
                values.put("name", "Сведение рук в кроссовере");
                values.put("target_muscle", "Грудь");
                values.put("involved_muscle", "Плечи");
                values.put("level", 1);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "Отличное изолирующее упражнение для мышц груди. Движения аналогичны разведению рук с гантелями лежа.");
                values.put("how_to_do", "1. Выставьте на блоках тросового тренажера необходимый вес, возьмите рукоятки троса в каждую руку. Встаньте строго по центру конструкции.\n2. Исходное положение: сделайте шаг и немного наклонитесь вперед. Разведите руки в стороны максимально растягивая грудные мышцы. Локти немного согнуты.\n3. На выдохе - по широкой дуге тяните рукояти к центру туловища. Следите, чтобы движение происходило только за счет плечевого сустава, руки и туловище должны быть неподвижны на протяжении всего упражнения.\n4. На вдохе - медленно вернитесь по в исходное положение до легкого растяжения в пекторальных.\n5. Выполните тягу нужное количество раз.");
                values.put("advice", "1. Корпус наклоняйте вперед примерно на 20 градусов. Так положение тела более устойчиво.\n2. Руки сводите перед собой и вниз. В нижней половине амплитуды сильнее работает низ груди. В верхней - верхняя часть груди.\n3. Когда поднимаете руки, локти должны смотреть назад и немного вверх. В нижней точке можете полностью выпрямлять руки.\n4. При выполнении упражнения следите, чтобы спина была прямая, грудь развёрнута, а лопатки сведены вместе. За счет этого грудные мышцы будут максимально растягиваться и сокращаться.\n5. Упражнение изолирующее, рекомендуется в тренировке его делать для проработки после 1 - 2 базовых упражнений.\n6. Изменяя наклон туловища, Вы можете переносить нагрузку на целевые мышцы: наклоняясь ниже, больше нагружается верх груди, меньше наклон, больше работает нижняя часть.");
                db.insert("exercise_store", null, values);

//exercise = 45
                values.put("name", "Сгибание рук с гантелями на наклонной скамье");
                values.put("target_muscle", "Бицепс");
                values.put("involved_muscle", "Плечи");
                values.put("level", 2);
                values.put("inventory", "Гантели, Силовая скамья");
                values.put("dumbbells", 1); //Гантели
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Сгибания рук с гантелями сидя на наклонной скамье не очень популярное, но эффективное упражнение для бицепса. Позволяет сильнее растянуть мышцы, чем при вертикально положении тела. Для выполнения этого упражнения кроме гантелей нужна наклонная силовая скамья. Внимательно следите за техникой выполнения упражнения.");
                values.put("how_to_do", "1. Установите спинку на силовой скамье под углом (рекомендуется 50-70 градусов) Возьмите в руки гантели с необходимым весом и сядьте на наклонную скамью.\n2. Исходное положение: руки выпрямлены, опущены вниз по сторонам туловища. Голова прижата к спинке скамьи.\n3. На выдохе: держа локоть неподвижно, сгибайте руки за счет усилия бицепса.\n4. На вдохе: верните руки в исходное положение.\n5. Сделайте требуемое количество повторов.");
                values.put("advice", "1. Наклон спинки скамьи ниже 50 градусов делать не стоит - при выполнении разгибания рук будут очень сильно растягиваться бицепсы - это может привести к растяжению сухожилий.\n2. На наклонной скамье поднимать гантели тяжелее чем в вертикальном положении, поэтому подбирайте вес гантелей для упражнения примерно на 20% меньше, чем вы делаете стоя.\n3. Движения должны быть плавными, без раскачиваний! Нельзя делать замах в нижней точке - это снизит эффективность упражнения, а также может привести к травме.\n4. Более удобно в нижней точке кисть поворачивать вовнутрь. В верхней точке если будете разворачивать наружу, то нагрузите внешнюю головку бицепса, если не будете разворачивать, то нагрузите внутреннюю головка бицепса и предплечье.\n5. Сгибать руки можно как вместе, так и попеременно.");
                db.insert("exercise_store", null, values);

//exercise = 46
                values.put("name", "Сведение ног в тренажере");
                values.put("target_muscle", "Бедра");
                values.put("involved_muscle", "Ягодицы");
                values.put("level", 1);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Очень популярное упражнение среди девушек, которые только начинают заниматься в тренажерном зале. Обычно выполняют в паре с");
                values.put("how_to_do", "1. Установливаем необходимый вес на тренажере.\n2. Садитесь на тренажер, ставьте ноги на подставки.\n3. Исходное положение - спина ровная, а руками держитесь за рукоятки, прижимая тело к скамье.\n4. На выдохе - сводим ноги вместе.\n5. На вдохе - разводим. Делаем эту фазу упражнения медленно.\n6. Сделайте сведение ног нужное количество раз.");
                values.put("advice", "1. Если очень трудно делать это упражнение (особенно на последних повторениях), можете помогать себе руками.\n2. Обязательно хорошо делайте разминку перед этим упражнением, растяните паховые связки. Чем подвижнее будут Ваши суставы, тем с большей амплитудой смодете выполнят упражнение.\n3. Упражнение изолирующее, поэтому делать его без других упражнений на ноги смысла нет. Также советуем его делать ближе к концу тренировки.");
                db.insert("exercise_store", null, values);

//exercise = 47
                values.put("name", "Разведение ног в тренажере");
                values.put("target_muscle", "Ягодицы");
                values.put("involved_muscle", "Бедра");
                values.put("level", 1);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Относится к одному из самых любимых упражнений девушек, которые только начинают заниматься в тренажерном зале. Отличается от");
                values.put("how_to_do", "1. Установить необходимый вес на тренажере.\n2. Садитесь на тренажер, ставьте ноги на подставки.\n3. Исходное положение - спина прямая, руками беритесь за рукоятки.\n4. На выдохе - разведите ноги.\n5. На вдохе - вернитесь в исходное положение.\n6. Сделайте разведение ног нужное количество раз.");
                values.put("advice", "1. Когда разводите ноги, задерживайтесь на секунду в этом положении. Разводите ноги маховым движением, а сводите плавно, не торопясь.\n2. До конца ноги можете не сводить, это сильнее нагрузит мышцы.\n3. Ставьте такой вес, чтобы сделать хотя бы 10 повторений. Хорошим эффектом от этого упражнения является жжение в мышцах.");
                db.insert("exercise_store", null, values);

//exercise = 48
                values.put("name", "Тяга за голову верхнего блока");
                values.put("target_muscle", "Широчайшие мышцы спины");
                values.put("involved_muscle", "Бицепс, Плечи");
                values.put("level", 2);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "Тяга за голову - упражнение для широчайших мышц спины. Требует большую подвижность плечевых суставов. Очень важно выполнять его правильно, если ваши суставы не позволяют дотянуть гриф до нужного уровня не согнув спину не делайте его - толку будет мало.");
                values.put("how_to_do", "1. Возьмите гриф широких хватом сверху. Сядьте на лавку отрегулированного под Ваш рост тренажера. Бедра расположите под валиками.\n2. Исходное положение - спина прямо, руки выпрямлены, стопы плотно прижмите к полу.\n3. На выдохе - тяните гриф вниз, за голову примерно до середины затылка, сводя при этом лопатки вместе. Можете задержаться в крайнем положении.\n4. На вдохе - верните гриф в исходное положение.\n5. Выполните повторение тяги необходимое количество раз.");
                values.put("advice", "1. При движении рукояти вниз немного подайте корпус вперёд (держа спину ровно) и сводите лопатки. Это поможет лучше нагрузить спину.\n2. Не беритесь за гриф сильно широко. Это очень снизит амплитуду тяги и тем самым снизит эффективность упражнения. Выбирайте ширину такой, чтобы в нижней точке предплечья были параллельны (это примерно 1.5 ширины плечей).\n3. Можно приматываться лямками к грифу. Этим вы уберёте нагрузку с предплечий и будете больше сконцентрированы на работе широчайших.\n4. Голову во время упражнения держите прямо и смотреть вперёд. Ещё раз повторяем - если горбитесь, то лучше не делайте это упражнение.");
                db.insert("exercise_store", null, values);

//exercise = 49
                values.put("name", "Подъем ног в упоре");
                values.put("target_muscle", "Пресс");
                values.put("involved_muscle", "Нижняя часть спины");
                values.put("level", 1);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "На пресс существует большое количество упражнений, но на самом деле эффективных упражнений не так много. Подъем ног в упоре (или в висе) как раз относится к наиболее эффективным и популярным, и по своей сути является скручиванием наоборот: низ тела скручивается к верхней части туловища. Для выполнения подъема ног нужен тренажер с опорами для локтей.");
                values.put("how_to_do", "1. Исходное положение - займите позицию в тренажере, возьмитесь за рукоятки. Руки согнуты под прямым углом. Плечи расправлены, поясница прижата к тренажеру, туловище выпрямлено.\n2. На выдохе - поднимайте колени вверх, в направлении к груди. Можете задержаться в крайней точке.\n3. На выдохе - вернитесь в исходное положение.\n4. Сделайте подъем ног необходимое число раз.");
                values.put("advice", "1. Не раскачивайтесь и не делайте рывков. Подъем ног выполняйте плавно и размеренно, напрягая мышцы пресса.\n2. Положение ног не важно при выполнении упражнения. Т.к. мышцы пресса тянут не ноги, а таз. Думайте во время выполнения о том, как можно выше поднять таз, а не ноги.\n3. Старайтесь таз не только поднимать, но и подавать (подкручивать) его вперед.\n4. Подъем ног можно выполнять не только прямо вверх, но и в стороны, что походит по технике на диагональные скручивания и прокачивает косые мышцы живота.\n5. Не сползайте вниз при выполнении упражнения! Если не можете, то попробуйте выполнять упражнение на прямых руках.\n6. Если будете ноги опускать в низу не до конца, то пресс будет работать более эффективно.");
                db.insert("exercise_store", null, values);

//exercise = 50
                values.put("name", "Тяга штанги к подбородку");
                values.put("target_muscle", "Плечи");
                values.put("involved_muscle", "Трапеции");
                values.put("level", 1);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Данное упражнение является базовым, акцентирует нагрузку на средние и задние пучки плеч, при выполнении узким хватом в работу включаются трапеции и немного бицепс. Эффект от тяги штанги напрямую зависит того, как высоко вы поднимете локти, а не штангу. В нижней части траектории движения работают плечи, а в верхней (выше уровня груди) трапециевидные мышцы. Упражнение выполняется как со свободным весом, так и в тренажере Смита.");
                values.put("how_to_do", "1. Возьмите штангу хватом сверху. Расстояние между руками 10-15 см.\n2. Исходное положение - поясница прогнута, спина прямая, немного наклонитесь вперед.\n3. На выдохе - тяните штангу в небольшом удалении от корпуса к подбородку до уровня груди. Осуществляйте движение за счет локтей, плечи опущены вниз. В верхней точке задержите штангу на пару секунд\n4. На выдохе - плавно опускайте штангу вниз.\n5. Сделайте нужное количество повторений тяги.");
                values.put("advice", "1. Штангу тяните как можно ближе к туловищу. Не по дуге, а по прямой траетории.\n2. Темп выполнения должен быть низким. Следите чтобы локти всегда были разведены в стороны.\n3. Широкий хват имеет преимущество над узким тем, что исключает бицепс из работы, но при этом уменьшает траекторию.");
                db.insert("exercise_store", null, values);

//exercise = 51
                values.put("name", "Сгибание ног в тренажере лежа");
                values.put("target_muscle", "Бедра");
                values.put("involved_muscle", "Икры");
                values.put("level", 1);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Упражнение предназначено для проработки задней поверхности бедра, является изолирующим - позволяет качественно загрузить бедро без участия поясничных и ягодичных мышц.");
                values.put("how_to_do", "1. Лягте животом на тренажер для сгибания ног так чтобы колени немного выступали за край скамьи, а фиксаторы были на ахилловом сухожилии.\n2. Исходное положение - ноги зафиксируете под валиками, возьмитесь за рукоятки.\n3. На выдохе - сокращайте бицепс бедра (поднимайте ноги).\n4. На вдохе - плавно растягивайте (опускайте ноги).\n5. Повторите сгибание ног нужное количество раз.");
                values.put("advice", "1. Удобно настройте тренажер. Валик должен упираться в поверхность чуть выше пятки, а колено должно чуть свешиваться с платформы.\n2. Не выполняйте упражнение с использованием сил инерции. Выполнение упражнени должно быть плавным, движения должны быть неторопливыми и контролируемыми.\n3. Ноги до конца не разгибайте.\n4. Поднимайте отягощение только усилием мышц бедра, не помогайте себе руками.");
                db.insert("exercise_store", null, values);

//exercise = 52
                values.put("name", "Приседания со штангой");
                values.put("target_muscle", "Квадрицепс");
                values.put("involved_muscle", "Ягодицы, Бедра");
                values.put("level", 3);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Приседание - одно из самых сложных упражнений в спорте. Оно задействует все тело, не только ноги. Способствует увеличению силы и массы, как ног, так и всего тела.");
                values.put("how_to_do", "1. Возьмите штангу и положите её на плечи.\n2. Исходное положение - встаньте в комфортную позицию для прыжка. Если представить, что сейчас прыгнешь вперед, то ноги возьмут ту ширину и изгиб оптимальный для приседаний.\n3. На вдохе - плавно опускайтесь вниз.\n4. На выдохе - вернитесь в исходное положение. Не разгибайте полостью колени, оставляйте их чуть согнутыми, контролируйте напряжение.\n5. Сделайте нужное количество приседаний.");
                values.put("advice", "1. Положение головы. Ни в коем случае нельзя смотреть себе под ноги. Вы не должны наклонять голову вперед, это движение округляет спину, что может привести к травме поясницы. Смотрите четко вперед, можно слегка вверх.\n2. Гриф кладется на дельты. Ни в коем случае, нельзя класть гриф на шею.\n3. Следите за коленями. Они не должны выходить за носки.\n4. Носки должны быть развернуты в сторону коленей.\n5. Чем глубже будете приседать, тем сильнее в работу включаются ягодицы. В нижней точке приседания тазобедренный сустав должен быть немного ниже коленного.\n6. Ширина постановки регулирует распределение нагрузки. Уже ноги - сильнее работает передняя часть бедра. Шире ноги - нагрузка ложится сильнее на внутреннюю поверхность бедра.\n7. Не отрывайте пятки от пола на протяжении выполнения всего упражнения.");
                db.insert("exercise_store", null, values);

//exercise = 53
                values.put("name", "Жим гантелей сидя");
                values.put("target_muscle", "Плечи");
                values.put("involved_muscle", "Трицепс");
                values.put("level", 2);
                values.put("inventory", "Гантели, Силовая скамья");
                values.put("dumbbells", 1); //Гантели
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Жим гантелей сидя является базовым упражнением. Акцентирует нагрузку на средние и боковые пучки плеч, также в работе задействованы трицепсы. Упражнение удобней делать сидя, но можно и");
                values.put("how_to_do", "1. Сядьте на отрегулированную силовую скамью, удерживая гантели в руках.\n2. Исходное положение - спина и поясница прижаты к спинке скамьи, поднимите гантели на уровень глаз. Разверните локти в стороны, предплечья вертикальны, ладони смотрят вперед, стопы уперты в пол.\n3. На выдохе - выжимайте гантели к верхней точке, немного сближая их. Задержитесь в верхней точке на пару секунд.\n4. На вдохе - вернитесь в исходное положение.\n5. Повторите жим гантелей необходимое число раз.");
                values.put("advice", "1. Для того чтобы акцентировать нагрузку на передний пучок дельт, необходимо к нижей точке плавно разворачивать руки ладонями друг к другу.\n2. Делаем паузу в верхней точке, но ни в коме случаи не делаем паузу внизу. Так как мышцы рефлекторно стремятся сильнее сократиться в ответ на растяжение. При паузе внизу эффект сократится\n3. Опускайте гантели как можно ниже, касаясь ими плеч, а выжимайте до конца - чем больше амплитуда жима, тем лучше.\n4. Для новичков можно делать жим руками поочередно.");
                db.insert("exercise_store", null, values);

//exercise = 54
                values.put("name", "Разгибание рук на блоке стоя");
                values.put("target_muscle", "Трицепс");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "Разгибание рук на блоке является изолирующее упражнение и направлено на проработку трицепса. Упражнение придает форму и делает трицепс рельефным. Может выполняться в нескольких вариантах (с V-образной рукоятью, с канатной рукоятью, с прямой рукоятью).");
                values.put("how_to_do", "1. Нагрузите тросовый тренажер необходимым весом. Возьмитесь за рукоять, присоединенную к тросу верхнего блока хватом сверху, немного уже ширины плеч.\n2. Исходное положение - потяните рукоять тренажера вниз и прижмите локти к туловищу. Слегка наклонитесь вперед и немного согните колени. Руки согнуты в локтях, предплечья параллельны полу и обращены к тренажеру, а часть рук от локтя и выше должна быть перпендикулярна полу.\n3. На выдохе: держа неподвижной часть руки от плеча до локтя, полностью разогните руки опуская рукоять вниз, касаясь бедер. Рука от плеча до локтя неподвижна. Задержитесь на секунду в этом положении.\n4. На вдохе: медленно верните рукоять исходное положение.\n5. Сделайте необходимое число повторов.");
                values.put("advice", "Наклоняйтесь вперед совсем немного - иначе вы будете вополнять упражнение не за счет напряжения мышц, а за счет веса своего тела.\nСледите за локтями! На протяжении всего упражнения они должны оставаться неподвижными.\nВо время разгибания рук на вертикальном блоке спина должна быть прямой, а грудь развернутой. Допускается для устойчивости одну ногу выносить вперед.");
                db.insert("exercise_store", null, values);

//exercise = 55
                values.put("name", "Скручивания на наклонной скамье");
                values.put("target_muscle", "Пресс");
                values.put("involved_muscle", "Нижняя часть спины");
                values.put("level", 1);
                values.put("inventory", "Силовая скамья");
                values.put("power_bench", 1); //Силовая скамья
                values.put("description", "Скручивания - основное упражнение для развития правильной и красивой формы мышц пресса. Скручивания можно выполнять как на полу, но сложнее это делать на наклонной скамье. Это упражнение прекрасно подойдет как разминка - для начала любой тренировки, так и для её окончания. Существует много вариантов этого упражнения, позволяющих регулировать нагрузку на разные части пресса.");
                values.put("how_to_do", "1. Займите положение на наклонной скамье - ноги согните в коленях, ступни зацепите за упоры.\n2. Исходное положение - лягте на спину, поясница прижата к скамье. Руки за головой (или перед грудью), кисти взяты в \"замочек\", локти разведены.\n3. На выдохе - усилием мышцы пресса переместите верхнюю часть туловища к коленям (туловище прижато или близко к коленям). Задержитесь на секунду.\n4. На вдохе -плавно вернитесь в исходное состояние.\n5. Повторить упражнение необходимое количество раз.");
                values.put("advice", "1. Делайте упражнение с достаточно большой амплитудой, но не касайтесь лопатками пола (скамьи), так как в таком положении мышцы пресса будут отдыхать и у упражнения снизится эффективность.\n2. Со временем вам станет легко выполнять это упражнение, и чтобы усложнить задачу вы можете делать его с блином на груди (или за головой, это немного сложнее), держа его руками.\n3. Выполняйте упражнение правильно, представьте, что вам нужно достать лбом ваши колени. Так пресс будет получать большую нагрузку.\n4. Чем больше наклон скамьи, тем скручивание делать сложнее.\n5. При выполнении скручиваний на наклонной скамье вам необходимо максимально сгорбить спину, чтобы заработал пресс. Если будете держать спину прямой, то будет работать в основном поясничный отдел.");
                db.insert("exercise_store", null, values);

//exercise = 56
                values.put("name", "Сведение рук в тренажере (бабочка)");
                values.put("target_muscle", "Грудь");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Изолирующее упражнение для мышц груди - т.е. в этом упражнении работает только один сустав - плечевой. Это позволяет сфокусировать работу на необходимой мышце - грудной. Отличительной особенностью также является то, что в конечной точке упражнения, когда вы сводите руки вместе, на мышцы груди ложится пиковое напряжение.");
                values.put("how_to_do", "1. Сядьте на сиденье тренажера, обхватите рукоятки тренажера.\n2. Исходное положение - поясницу и спину прижмите к его спинке, лопатки соедините.\n3. На выдохе - двигайте руки вперед, за счет усилия грудных мышц, пока не соедините руки перед собой. Остановитесь в этом положении на несколько секунд.\n4. На вдохе - верните руки в исходное положение.\n5. Сделайте нужное количество повторов.");
                values.put("advice", "1. Плечи и поясница должны быть всегда прижаты к спинке тренажёра, а лопатки сведены вместе. Только так упражнение делается правильно и нагружает грудь.\n2. Локти должны быть напрвлены в стороны (не вниз).\n3. Отрегулируйте тренажер под себя - высота сиденья должна быть такой, чтобы ручки были на середине груди.\n4. При выполнении упражнения можно не отводить руки сильно назад - достаточно возвращать руки до уровня груди.\n5. Не ставьте это упражнение в начале тренировки - оно предназначено для загрузки груди после базовых упражнений.");
                db.insert("exercise_store", null, values);

//exercise = 57
                values.put("name", "Подъемы гантелей через стороны стоя");
                values.put("target_muscle", "Плечи");
                values.put("involved_muscle", "Трапеции");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Классическое упражнение для развития дельт - мышц, окружающих плечевые суставы. Больше всего в подъеме гантелей через стороны работают средние дельты. Вы можете выполнять разведение гантелей в стороны одной рукой - попеременно (оперевшись боком на наклонную скамью или удерживаясь свободной рукой за опору).");
                values.put("how_to_do", "1. Возьмите гантели с установленным необходимым Вам весом.\n2. Исходное положение: встаньте прямо, ноги немного уже или на ширине плеч. Руки вдоль тела, немного согнуты в локтях, ладони внутрь.\n3. На выдохе - сохраняя тело прямым, поднимайте гантели в стороны. При этом немного поворачивайте руки с гантелями (представьте, что вы держите кружки с водой и выливаете из них воду). Поднимайте руки немного уровня плеч.\n4. На вдохе - медленно верните руки в исходное положение.\n5. Сделайте необходимое количество повторений.");
                values.put("advice", "1. Следите за тем, чтобы поднимать гантели чётко в стороны. Если будете выносить руки вперёд, то перенесете нагрузку на передние дельты.\n2. Не опускайте локти! В верхней точке упражнения они должны быть обращены назад, а не вниз и находиться на одном уровне с кистями.\n3. Поднимать гантели следует до уровня плеч, можно немного выше. Сильное поднимание рук включает в работу трапеции.\n4. При разведении гантелей в стороны спину держите прямой, а плечи расправлеными.\n5. Не поднимайте плечи вверх во время выполнения подъемы гантелей - так нагрузка ляжет на трапецию, а не на плечи.\n6. Выполняйте упражнение плавно, не раскачивайтесь и не пытайтесь закинуть гантели вверх.");
                db.insert("exercise_store", null, values);

//exercise = 58
                values.put("name", "Отжимания от пола");
                values.put("target_muscle", "Грудь");
                values.put("involved_muscle", "Плечи, Трицепс");
                values.put("level", 1);
                values.put("inventory", "Отсутствует");
                values.put("description", "Незаменимое и простое упражнение -не понадобиться ничего, кроме земли(пола) и Вашего тела. Отжимания - базовое (или многосуставное) упражнение. По своей сути, отжимания - это тот же классический жим лежа наоборот. Отличное упражнение для начинающих.");
                values.put("how_to_do", "1. Лягте на пол. Примите упор лежа, удерживая тело на выпрямленных руках.\n2. Исходное положение - руки немного шире плеч, ладони на одном уровне с плечами.\n3. На вдохе - сохраняя тело прямым, сгибайте руки, пока грудью не коснетесь пола.\n4. На выдохе - вернитесь в исходное положение.\n5. Повторите нужное число раз.");
                values.put("advice", "1. От положения рук сильно зависит то, какие мышцы задействованы: руки поставлены высоко или узко - больше работает трицепс, чем шире руки - тем сильнее задействована грудь.\n2. Хотите чтобы руки были задействованы минимально - делайте отжимания, используя какую-нибудь подставку для ног - если ноги будут выше плечей, то нагрузка ложится в основном на грудь и плечи.\n3. Попробуйте поиграть с амплитудой упражнения - если будете делать отжимания в верхней части амплитуды, то работают трицепсы, если в нижней - грудь.\n4. Если вы делаете уже много повторений, то используйте отягощения. Подбирайте его таким, чтобы выполнить 8-10 раз.");
                db.insert("exercise_store", null, values);

//exercise = 59
                values.put("name", "Планка");
                values.put("target_muscle", "Пресс");
                values.put("involved_muscle", "Плечи, Трапеции, Нижняя часть спины");
                values.put("level", 1);
                values.put("inventory", "Отсутствует");
                values.put("description", "Планка интересное и набирающее популярность упражнение - его цель создать крепкий мышечный корсет тела - живот, поясницу. Упражнение базовое общеукрепляющим для многих других групп мышц тела. Планка относится к классу статических упражнений, т.е. не требует движения суставов.");
                values.put("how_to_do", "1. Лягте на пол (при наличии используйте гимнастический коврик)\n2. Исходное положение - вытяните тело, опираясь на две опорные точки -предплечья (держите их под точным углом в 90 градусов и параллельно друг другу на ширине плеч) и носки стоп. Ноги вместе, взгляд направлен в пол. Плечи отведены назад и вниз.\n3. Держите тело прямым - так, чтобы голова, позвоночник и ноги были строго на одной линии. Напрягите мышцы брюшного пресса и ягодиц и контролируйте неподвижность тела.\n4. Продержитесь в таком положении максимально долго.\n5. Выполните необходимое число повторений.");
                values.put("advice", "Существует большое число вариаций выполнения упражнения. Самые популярные- с выносом руки вперед, боковая с поворотом, с поднятием одной ноги, на прямых руках. Увеличивайте время выполнения упражнения планка и усложняйте его!");
                db.insert("exercise_store", null, values);

//exercise = 60
                values.put("name", "Жим ногами в тренажёре");
                values.put("target_muscle", "Квадрицепс");
                values.put("involved_muscle", "Ягодицы, Бедра");
                values.put("level", 1);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Жим ногами относится к базовым упражнениям для мышц ног. Большое его преимущество - позволяет без привлечения спинных (в том числе поясничных) мышц, проработать большой объем мышц бедер. Поэтому жим ногами отлично подойдёт тем людям, которые испытывают проблемы со спиной - здесь нет осевой нагрузки на позвоночник. Чтобы выполнять его Вам необходим тренажер с платформой на салазках. Суть упражнения в сгибании/разгибании коленного сустава, под весом платформы.");
                values.put("how_to_do", "1. Установите необходимую нагрузку на платформу тренажера. Лягте на тренажер, ноги расположите в середине платформы на ширине плеч.\n2. Исходное положение - выжмите немного платформу и руками снимите её со стопоров (руки удерживают поручни во время всего упражнения). Голову прижмите к спинке.\n3. На вдохе - медленно сгибайте ноги вниз до угла 90 градусов в коленном суставе.\n4. На выдохе - разгибайте колени, толкая вес пятками, возвращая платформу в исходное положение.\n5. Повторите жим необходимое количество раз.");
                values.put("advice", "1. Ступни должны всей площадью упираться в платформу, а толчок делается пятками. Колени всегда параллельны друг другу, не заваливайте их внутрь или наружу. Ступни с коленями двигаются в одной плоскости.\n2. Чем шире вы ставите ноги в упор платформы и разводите колени в стороны, тем большая нагрузка ложится на внутреннюю часть бедра. Если ставите ноги и колени уже ширины плеч, то больше будет работать передняя поверхность бедра.\n3. Чем выше на платформе вы устанавливаете ноги, тем больше работают ягодицы, чем ниже - бёдра.\n4. При опускании вниз следите, чтобы в нижней точке таз не отрывался от опоры - иначе можете травмировать поясницу.\n5. Попробуйте во время жима не выпрямлять ноги до конца - это сделает нагрузку более высокой.\n6. Хоть жим ногами в тренажере и является базовым упражнением, заменять им приседания со штангой не рекомендуется. Это оправдывается только в том случае, если из-за травм вы не можете приседать.");
                db.insert("exercise_store", null, values);

//exercise = 61
                values.put("name", "Французский жим лёжа");
                values.put("target_muscle", "Трицепс");
                values.put("involved_muscle", "Плечи");
                values.put("level", 1);
                values.put("inventory", "Гантели, Штанга");
                values.put("dumbbells_barbell", 1); //Гантели-Штанга
                values.put("description", "Одно из самых распространенных упражнений для тренировки трицепса. Но опасное. Точечная высокая нагрузка на локти может нанести травму. Удобно использовать штангу с EZ-грифом, но если её нету, то можно использовать с обычным грифом или гантели.");
                values.put("how_to_do", "1. Возьмите штангу хватом сверху немного уже уровня плеч. Лягте на горизонтальную скамью со штангой. Если используете гантели, то руки держите параллельно друг другу.\n2. Исходное положение - выпрямите руки в локтях и немного отклоните их назад за голову (на 20 - 30 градусов).\n3. На вдохе - удерживая плечи неподвижными, согните руки и поднесите штангу к голове. Угол в локтевом суставе быть 90 градусов.\n4. На выдохе - верните штангу в исходное положение.\n5. Сделайте упражнение нужное число раз.");
                values.put("advice", "1. Локти и плечи должны быть зафиксированы. Движение во время французского жима только в локтевом суставе. Подбирайте такой вес, при котором вы делаете упражнение правильно! Если вес мешает вам держать локти неподвижными - уменьшите его!\n2. Старайтесь полностью выпрямляйте руки в локтях в верхней точке упражнения.\n3. Оптимальная ширина хвата - 20 - 30 см. В таком положении трицепс загружен оптимально, и нагрузка на суставы равномерна.\n4. Для французского жима необязательно нужна силовая скамья. Его вполне удобно делать прямо на полу!\n5. Ни в коем случае не поднимайте штангу с пола из-за затылка уже после того, как легли на лавку. Или берите её в руки перед тем, как ложиться, или просите подать её Вам.");
                db.insert("exercise_store", null, values);

//exercise = 62
                values.put("name", "Армейский жим");
                values.put("target_muscle", "Плечи");
                values.put("involved_muscle", "Грудь, Трапеции, Трицепс");
                values.put("level", 1);
                values.put("inventory", "Гантели, Штанга");
                values.put("dumbbells_barbell", 1); //Гантели-Штанга
                values.put("description", "Армейский жим - одно из самых эффективных упражнений для мышц плечевого пояса. Главная нагрузка приходится на передние и средние пучки дельт. Очень важно выполнять жим штанги стоя правильно, иначе эффективность резко снижается.");
                values.put("how_to_do", "1. Возьмитесь за гриф немного шире плеч хватом сверху. Поднимите штангу на грудь.\n2. Исходное положение: штанга находится на груди, локти немного выдвинуты вперед (так, чтобы ладони смотрели точно вверх), спину держите прямой. Ноги на ширине плеч, немного согнуты в коленях.\n3. На выдохе - плавно выжмите штангу вверх, пока не зафиксируете её на прямых руках.\n4. На вдохе - опустите штангу обратно на грудь.\n5. Выполните необходимое количество повторов.");
                values.put("advice", "1. Если при выполнении армейского жима ваши руки начинают гулятьснизте рабочий вес.\n2. При выполнении жима штанги стоя следите, чтобы таз был немного оттопырен (немного за пятками). Это позволит избежать излишний прогиб в пояснице. Если будете сильно подавать таз вперёд или прогибаться, то можете травмировать поясницу.\n3. Штангу жмите так, чтобы в верхней точке гриф был прямо над головой, можно слегка сзади.\n4. В нижней точке касайтесь грифом верхнюю часть груди. Паузу делать НЕ нужно.\n5. В нижней точке локти должны быть слегка выведены вперёд - немного впереди грифа.\n6. Жать стараться по прямой траектории. Чтобы не обводить грифом голову, просто отводите её немного назад.");
                db.insert("exercise_store", null, values);

//exercise = 63
                values.put("name", "Тяга штанги в наклоне");
                values.put("target_muscle", "Широчайшие мышцы спины");
                values.put("involved_muscle", "Бицепс, Плечи, Средняя часть спины, Нижняя часть спины");
                values.put("level", 1);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Большинство упражнений для широчайших мышц спины не дают нагрузку в облегченном варианте использование различных тренажеров делают работу с широчайшими практически изолированной (например, тяга в горизонтальном блоке) - и только тяга штанги в наклоне к поясу позволяет качественно нагрузить не только широчайшие, но и много других мышц спины и делает его основным при работе над спиной. Все профессиональные тяжелоатлеты включают это упражнение в свои программы тренировок.");
                values.put("how_to_do", "1. Возьмите штангу хватом сверху (ладони обращены вниз) немного шире плеч.\n2. Исходное положение - ноги на ширине плеч, немного согнуты в коленях (10 - 20 градусов) наклонитесь вперед, тело при этом должно быть практически параллельно полу и перпендикулярно бедрам. Держите спину прямой. Штанга должна находиться перед вами на вытянутых руках.\n3. На выдохе - держа туловище неподвижно, тяните штангу к себе. При этом максимально отводите назад локти и плечи, сведите лопатки. Притянув гриф к поясу, напрягите мышцы спины и на секунду задержитесь в этом положении.\n4. На вдохе - медленно верните штангу в исходное положение.\n5. Сделайте необходимое количество повторений.");
                values.put("advice", "1. Самые главные ошибки тяги штанги в наклоне - горбатая спина и слабый наклон туловища. Наклон должен быть примерно 70 - 80 градусов относительно пола, а ноги должны быть согнуты.\n2. Штанга должна ходить только вверх и вниз, а не гулять вперёд - назад. Если вы делаете упражнение правильно, то штанга будет подниматься как раз к середине живота.\n3. Брать штангу прямым хватом или обратным, сильно значения не имеет. У обратного хвата преимущество - он позволяет ближе держать локти к туловищу, за счет чего широчайшие будут сокращаться сильнее. Также попробуйте выполнить это упражнение со штангой с изогнутым грифом.\n4. При выполнении тяги штанги к поясу кроме рук у Вас обязательно должны работать лопатки - когда штанга поднимается вверх лопатки сводятся вместе, а грудь расправляется.");
                db.insert("exercise_store", null, values);

//exercise = 64
                values.put("name", "Тяга верхнего блока к груди");
                values.put("target_muscle", "Широчайшие мышцы спины");
                values.put("involved_muscle", "Бицепс, Плечи, Средняя часть спины");
                values.put("level", 1);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "Тяга верхнего блока используется для нагрузки мышц спины. Упражнение представляет собой подтягивания - только не Вы тянетесь к перекладине, а рукоять тренажера тянется к Вам. Существует большое число модификаций данного упражнения, которые различаются шириной хвата - при узком и среднем (на ширине плеч) максимально загружены широчайшие мышцы спины, при широком - Вторая часть спины и наружние пучки широчайших мышц.");
                values.put("how_to_do", "1. Прикрепите к верхнему блоку тросового тренажера необходимый по ширине гриф. Отрегулируйте упор для ног под Ваш рост и сядьте на скамью. Возьмите гриф хватом сверху\n2. Исходное положение: держа вытянутыми руками гриф, прогнитесь в пояснице, туловище отклоните назад примерно на 20 градусов, грудь подайте вперед.\n3. На выдохе - тяните гриф к верхней части груди, при этом отводя назад плечи и сводя лопатки вместе. Сделайте паузу в этом положении.\n4. На вдохе - медленно верните гриф в исходное положение, полностью выпрямляя руки, напрягая мышцы спины.\n5. Выполните необходимое количество повторений;");
                values.put("advice", "1. Садитесь как можно ближе к снаряду, для того, траектория движений грифа была максимально близкой к вертикальной - трос и и копчик должны быть всё время на одной линии.\n2. Не бросайте гриф при движении грифа вверх, всегда напряжением мышц контролируйте движение, стараясь добиться плавного движения.\n3. При подъёме вверх подавайте корпус немного вперёд, это поможет хорошо растянуть работающие мышцы. При движении вниз прогибайте спину, сводите лопатки вместе. Всё это нужно для того, чтобы сильнее растягивались и сокращались широчайшие мышцы.\n4. Если вы работаете с большими весами, то можете приматывать руки к ручке лямками. Это позволит не задумываться о предплечьях, и эффективно работать мышцами спины.\n5. Мысленно сконцентрируйтесь на лопатках - старайтесь выполнить тягу верхнего блока как бы ими.");
                db.insert("exercise_store", null, values);

//exercise = 65
                values.put("name", "Тяга нижнего (горизонтального) блока");
                values.put("target_muscle", "Вторая часть спины");
                values.put("involved_muscle", "Бицепс, Плечи, Широчайшие мышцы спины");
                values.put("level", 1);
                values.put("inventory", "Тросовые тренажеры");
                values.put("rope_trainers", 1); //Тросовые тренажеры
                values.put("description", "Тяга горизонтального блока задействует те же мышцы, что и упражнение тяга штанги в наклоне, и обладает положительными особенностями: снимается нагрузка с коленей и позвоночника. Для выполнения тяги понадобится нижний блок с прикрепленной к нему V-образной рукояткой - обычно используют именно её, но также можно прикреплять любые другие рукоятки, от них зависит на какие мышцы больше пойдет нагрузка:");
                values.put("how_to_do", "1. Сядьте на тренажер, упритесь ногами в подставку. При выполнении упражнения спину держите прямой.\n2. Исходное положение: слегка согните ноги в коленях, возьмите рукоятку, отклоняйтесь назад, пока туловище не будет перпендикулярно ногам, подайте грудь вперед.\n3. На выдохе- держа туловище неподвижно, тяните рукоятку на себя до тех пор, пока кисти не коснутся брюшного пресса. Задержитесь на пару секунд в этом положении, напрягая мышцы спины.\n4. На вдохе - медленно выпрямляйте руки, и немного подайте корпус вперед. Руки при этом не выпрямляйте до конца, чтобы более эффективно нагрузить спину.\n5. Выполните требуемое количество повторений.");
                values.put("advice", "1. Подбирайте такой вес, чтобы могли выполнить 6 - 8 повторений.\n2. В крайней точке упражнения (ручка притянута к животу), спина должна быть прямой, а лопатки сведены вместе.\n3. Когда вы отпускаете ручку от себя, подайтесь вперед, не сдвигая ягодицы и держа спину прямо - это позволит сильнее растянуть мышцы спины.\n4. Не отклоняйтесь назад во время тяги на нижнем блоке - это включает в работу второстепенные мышцы, что снижает эффективность упражнения.");
                db.insert("exercise_store", null, values);

//exercise = 66
                values.put("name", "Разведение рук с гантелями лежа");
                values.put("target_muscle", "Грудь");
                values.put("involved_muscle", "-");
                values.put("level", 2);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Одно из основных упражнений для мышц груди. Разведение рук с гантелями придает груди рельефность и выпуклую форму.");
                values.put("how_to_do", "1. Держа гантели в руках, лягте на скамью, ноги уприте в пол.\n2. Исходное положение: поднимите гантели вверх. Руки немного согнуты в локтях. Угол в локтевом суставе необходимо зафиксировать и держать неизменным во время выполнения упражнения.\n3. На вдохе - по широкой дуге разведите руки в стороны только за счет плечевого сустава. Опускайте гантели до уровня плеч или немного ниже.\n4. На выдохе - усилием мышц груди сведите гантели над собой по обратной траектории.\n5. Выполните необходимое количество повторений.");
                values.put("advice", "1. Следите, чтобы руки были немного согнуты (примерно 150 градусов) - так упражнение будет максимально эффективно и безопасно.\n2. Локти, во время разведения рук, должны смотреть вниз, а не вперёд, Иначе нагрузка будет ложиться не плечи, что может привести к травме плечевых суставов.\n3. Не используйте тяжелые гантели - вес должен быть такой, чтобы вы смогли выполнить хотя бы 10 повторений. Это поможет избежать травмы плечевых суставов.\n4. Для максимального эффекта растяжения грудных мышц следите за положением локтей. Чем локти ниже, тем сильнее растянуты мышцы груди. При этом, для избежания травм, не опускайте гантели ниже уровня плечей.\n5. Не забывайте про уровень наклона скамьи, чтобы сделать акцент на разные области грудных мышц.");
                db.insert("exercise_store", null, values);

//exercise = 67
                values.put("name", "Жим гантелей лежа");
                values.put("target_muscle", "Грудь");
                values.put("involved_muscle", "Плечи, Трапеции");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Это упражнение направлено на ту же группу мышц, что и");
                values.put("how_to_do", "1. Возьмите гантели в каждую руку, сядьте с ними на горизонтальную силовую скамью. Положите гантели на бедра и после этого лягте на скамью.\n2. Исходное положение: спина прогнута в пояснице, стопы ног упираются в пол. Руки с гантелями находятся на уровне плечей, грифы гантелей расположены на одной линии.\n3. На выдохе - поднимите руки вверх, немного сводя гантели, максимально сокращая грудную мышцу.\n4. На выдохе - вернитесь в исходное положение, максимально растягивая грудь.\n5. Повторите упражнение нужное число раз.");
                values.put("advice", "1. Во время выполнения упражнения следите за траекторией - гантели должны двигаться только в одной плоскости.\n2. Если в верхней точке разворачивать руки ладонями друг к другу и сильнее сводить гантели, то ваши грудные мышцы сократятся сильнее.\n3. Нижняя точка жима гантелей - когда гантели коснутся плеч. Не ниже.\n4. Полностью выпрямляя локти, вы включаете в работу трицепсы рук. В зависимости от необходимого результата выбирайте технику выполнения.\n5. Чтобы не нанести себе травму, по окончании упражнения вставайте следующим образом: немного приподнимите ноги, гантели положите на бедра и после этого вставайте, перенося вес гантелей на ноги.");
                db.insert("exercise_store", null, values);

//exercise = 68
                values.put("name", "Становая тяга классическая");
                values.put("target_muscle", "Нижняя часть спины");
                values.put("involved_muscle", "Предплечья, Квадрицепс, Трапеции, Широчайшие мышцы спины, Ягодицы, Бедра");
                values.put("level", 2);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Классическая становая тяга - отличное упражнение для стимуляции мышечного роста большой группы мышц. Одно из трех упражнений в пауэрлифтинге. Однако, сильно загружает мышцы поясницы, поэтому будьте осторожны и внимательно следите за плавностью движений, чтобы не получить травму.");
                values.put("how_to_do", "1. Исходное положение - станьте вплотную к грифу штанги, ноги немного уже плеч. Присядьте и возьмите штангу хватом сверху. Плечи должны находиться ровно над грифом штанги. Спину держите ровно.\n2. На выдохе - тяните штангу вверх, выпрямляя тело. Грифом штанги скользите вдоль ног. Полностью выпрямитесь, при этом сведите лопатки вместе.\n3. На вдохе - начните движение в обратном направлении. При этом сначала отведите таз назад. Скользите грифом вдоль тела, сгибая спину. После того, как пройдете колени, начните сгибать ноги, держа при этом спину прямо. Коснитесь штангой пола.\n4. Выполните необходимое число повторений.");
                values.put("advice", "1. Ступни обязательно ставьте под гриф штанги (примерно на 1/3 от длины стопы). Так Ваш центр тяжести во время выполнения упражнения будет расположен правильно и не даст Вам раскачиваться.\n2. Вес штанги подбирайте такой, чтобы не выгибать спину во время становой тяги. Не можете держать спину прямо - обязательно облегчите нагрузку. Иначе Вы рискуете получить проблемы с позвоночником.\n3. Если Вы испытываете боли в спине или поясницы, то если и будете выполнять становую тягу, то перед этим надо хорошенько размяться (сделать несколько подходов гиперэкстензии).\n4. Используйте лямки чтобы снизить нагрузку на запястья. Не стоит выполнять становую тягу разнохватом (одна рука прямой хват, другая - обратный) при выполнении ваши мышцы не будут работать симметрично.");
                db.insert("exercise_store", null, values);

//exercise = 69
                values.put("name", "Приседания в гакк-тренажере");
                values.put("target_muscle", "Квадрицепс");
                values.put("involved_muscle", "Ягодицы, Бедра");
                values.put("level", 2);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Принцип действия гакк-тренажера придумал русский борец и штангист Георг Гаккеншмидт. Конструкция тренажера проста: это стальная рама, оснащенная салазками, ходящими вниз-вверх с подплечниками. Большое преимущество гакк приседаний относительно классических - отсутствие нагрузки на позвоночник и оптимальные углы ног, за счет чего нагрузка максимально ложится на четырехглавую мышцу бедра.");
                values.put("how_to_do", "1. Исходное положение: ноги на середине платформы, спина прижата к спинке тренажера, поясница прогнута. Кисти крепко держатся за рукояти, голова на подголовнике. Снимите удерживающие стопора на тренажере.\n2. На вдохе - медленно опускайтесь вниз, до прямого угла в колене.\n3. На выдохе - вернитесь в исходное положение.\n4. Повторите заданное количество раз.");
                values.put("advice", "1. Колени на протяжении выполнения гакк писеданий должны находиться на уровне ступней.\n2. Акцентируйте усилие на пятки, а не на носки - так квадрицепс работает правильнее.\n3. От положения ног на платформе зависит нагрузка на работающие мышцы:<br/> - широкая постановка - внутренняя поверхность бедра и квадрицепс;<br/> - узкая постановка - передняя поверхность бёдер;<br/> - высокая постановка - ягодицы;<br/> - низкая постановка - передняя поверхность бедра.\n4. Следите за тем, чтобы плечи и таз были прижаты к спинке, а спина была слегка погнута в пояснице - так вы обезопасите позвоночник.");
                db.insert("exercise_store", null, values);

//exercise = 70
                values.put("name", "Жим штанги лёжа");
                values.put("target_muscle", "Грудь");
                values.put("involved_muscle", "Плечи, Трицепс");
                values.put("level", 1);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Базовое упражнение для развития мышц груди. Основная нагрузка ложится на середину груди, также активно задействованы трицепс и дельты.");
                values.put("how_to_do", "1. Исходное положение: лягте спиной на горизонтальную скамью. Спина прогнута в пояснице, лопатки соединены, ноги под прямым углом в упоре. Возьмите штангу таким хватом, чтобы в нижней точке межу плечом и предплечьем был прямой угол. Держите её прямо над собой, на уровне плечей. Лифтерский вариант отличается тем, что необходимо практически встать на мост - ступни находятся в упоре примерно на уровне таза, который прижат к скамье.\n2. На вдохе - опускайте штангу вниз, пока не коснетесь середины груди. Задержитесь ненадолго.\n3. На выдохе - поднимите штангу в исходное положение. Старайтесь выполнять упражнение только усилиями мышц груди.\n4. Проделайте необходимое количество повторений.\n5. Верните штангу на стойку.");
                values.put("advice", "1. Ложитесь на скамью так, чтобы глаза находились под грифом штанги. Ляжете слишком глубоко - будете задевать стойки при жиме. Если наоборот - будет очень неудобно брать и ставить штангу со стойки.\n2. При работе с большими весами используйте кистевые бинты, даже если они у Вас сильные. Постоянная высокая нагрузка может привести к болям в кистях.\n3. Никогда не делайте жим, если Вы один в зале. Это может привести к печальным последствиям. В идеале просите кого-нибудь подстраховать.\n4. Во время упражнения не бросайте штангу на грудь. Все движения должны быть контролируемы и плавны.\n5. Старайтесь как бы оттолкнутся от штанги - так техника упражнения и работа мышц будет более правильной.");
                db.insert("exercise_store", null, values);

//exercise = 71
                values.put("name", "Приседания с гантелями");
                values.put("target_muscle", "Ягодицы");
                values.put("involved_muscle", "Квадрицепс, Бедра");
                values.put("level", 1);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Хорошо подходит начинающих атлетов для разучивания техники приседаний. Также плюсом данного упражнения является отсутствие прямой нагрузки на позвоночник.");
                values.put("how_to_do", "1. Исходное положение: возьмите гантели в руки, ладони внутрь. Станьте прямо, ноги на ширине плеч. При выполнении упражнения голову удерживать приподнятой, а спину прямой.\n2. На вдохе - медленно опускайтесь, сгибая колени, пока бедра не станут параллельны полу. Следите, чтобы колени не перекрывали носки ног - чтобы не перегружать коленный сустав.\n3. На выдохе - поднимайтесь в исходное положение, стараясь отталкиваться от пола пятками.\n4. Выполните необходимое количество повторений.");
                values.put("advice", "1. Ноги ставьте на ширине плеч или немного уже. Но никак не шире плеч.\n2. Наклонять спину во время приседа нужно не ниже чем на 45 градусов. Иначе больше работать будет спина, а не ноги.\n3. Если приседаете с большим весом, то для удобства воспользуйтесь лямками.\n4. Пятки во время выполнения приседания с гантелями не отрывайте от пола. Если совсем не получается их удерживать прижатыми, то подложите небольшую подставку высотой 1-2 см.");
                db.insert("exercise_store", null, values);

//exercise = 72
                values.put("name", "Тяга одной гантели в наклоне");
                values.put("target_muscle", "Вторая часть спины");
                values.put("involved_muscle", "Бицепс, Плечи, Широчайшие мышцы спины");
                values.put("level", 3);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Довольно сложное упражнение, так как необходимо соблюдать много нюансов. Упражнение не подходит для новичков - сложно научиться выполнять его правильно. Для выполнения упражнения вам понадобится силовая скамья (или что-нибудь похожее на неё).");
                values.put("how_to_do", "1. Исходное положение: правым коленом встаньте на скамью. Затем наклонитесь (чтобы туловище было почти параллельно полу) и упритесь в край скамьи правой рукой. Левой рукой поднимите гантель, ладонь обращена к туловищу. Спину держите прямо.\n2. На выдохе - тяните гантель к пояснице за счет сокращения мышц спины (руки не должны участвовать).Всё туловище остается неподвижным. Старайтесь поднимать локоть повыше и не отводите руку в сторону.\n3. На вдохе - медленно верните гантель в исходное положение.\n4. Повторите упражнение необходимое количество раз.\n5. Смените сторону, и повторите упражнение другой рукой.");
                values.put("advice", "1. В нижней точке можно разворачивать гантель перпендикулярно скамье, при этом немного опуская плечо - это лучше растянет широчайшую мышцу.\n2. Правильно тянуть гантель так, чтобы кисть поднималась к талии. Локоть в сторону отставлять нельзя. Он должен смотреть вверх.\n3. Это упражнение многие относят к базовым. Вес следует взять такой, чтобы Вы могли выполнить 6 - 12 повторений.\n4. При использовании гантелей больших весов рекомендуется использовать лямки, чтобы разгрузить предплечья.");
                db.insert("exercise_store", null, values);

//exercise = 73
                values.put("name", "Подтягивания широким хватом");
                values.put("target_muscle", "Широчайшие мышцы спины");
                values.put("involved_muscle", "Бицепс, Средняя часть спины");
                values.put("level", 2);
                values.put("inventory", "Другие тренажеры");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Данное упражнение сосредоточено на проработку широчайшей мышцы спины и требует только наличие турника. Можно подтягиваться как к груди, так и за голову. При подтягивании за голову в работе участвует еще задняя дельта.");
                values.put("how_to_do", "1. Исходное положение: возьмитесь за перекладину турника широким хватом сверху.\n2. На выдохе - выполните подтягивание, пока перекладина не окажется за шеей (если подтягиваетесь к груди, то коснуться перекладины верхней частью груди). Задержитесь в верхней точке. Постарайтесь максимально напрягать мышцы спины в верхней точке упражнения.\n3. На выдохе - медленно вернитесь в исходное положение.\n4. Сделайте необходимое число повторений.");
                values.put("advice", "1. Верхняя часть тела на всем протяжении выполнения упражнения неподвижна ─ двигаются только руки. Старайтесь подтягиваться за счет мышц спины.\n2. Ширина хвата должна быть такой, чтобы предплечья в верхней точке подтягивания были параллельны друг другу. Так широчайшие мышцы смогут получать наилучшую нагрузку.\n3. В подтягивании широким хватом за голову не стоит горбиться вверху - надо сводить лопатки, корпус подавать чуть вперед, а голову не наклонять вперёд. Если Вы из-за плохой подвижности плечевых суставов всё-таки горбитесь, то лучше подтягивайтесь только к груди.");
                db.insert("exercise_store", null, values);

//exercise = 74
                values.put("name", "Выпады с гантелями");
                values.put("target_muscle", "Квадрицепс");
                values.put("involved_muscle", "Ягодицы, Бедра, Икры");
                values.put("level", 2);
                values.put("inventory", "Гантели");
                values.put("dumbbells", 1); //Гантели
                values.put("description", "Целевые мышцы данного упражнения - передняя поверхность бедра и ягодицы. Может выполняться без дополнительной нагрузки, только с весом тела.");
                values.put("how_to_do", "1. Исходное положение: возьмите в руки две гантели по бокам от тела и встаньте прямо, ладони внутрь;<p></p>\n2. На вдохе - сделайте широкий шаг вперёд правой ногой, удерживая тело прямо. Длина шага должна быть такой, чтобы голень правой ноги и бедро левой были перпендикулярны полу.\n3. На выдохе - усилиями мышц ног поднимите тело в исходное положение.\n4. Выполните упражнение другой ногой. Повторите нужное число раз.");
                values.put("advice", "1. Выполнять данное упражнение шагая смысла нет.\n2. Хотите усложнить упражнение и сделать его эффективнее? Делайте выпад на возвышение (10 - 20 см). Так мышцы ягодиц будут растягиваться сильнее.\n3. Использование гантелей отлично подходит для домашнего пользования. Также будет лучше для тех, кто испытывает проблемы со спиной (нагрузка на позвоночник меньше) и координацией (равновесие держать легче).\n4. Для более устойчивого положения пробуйте ставить ноги не на одной линии, а немного в стороны.\n5. Задняя нога при усилии должна быть на носке - чтобы основной вес тела переместить на переднюю (работающую) ногу.\n6. Если вам тяжело выполнять это упражнение, то можете попробовать облегчённый вариант: гантелю берете в одну руку, а другой рукой держитесь за опору, помогая себе в процессе выполнения упражнения.");
                db.insert("exercise_store", null, values);

//exercise = 75
                values.put("name", "Становая тяга со штангой на прямых ногах");
                values.put("target_muscle", "Бедра");
                values.put("involved_muscle", "Нижняя часть спины, Ягодицы");
                values.put("level", 2);
                values.put("inventory", "Штанга");
                values.put("barbell", 1); //Штанга
                values.put("description", "Отличное упражнение на заднюю поверхность бедра и ягодицы. Поэтому так популярно среди женщин. Также становая тяга известна как \"румынская\" или \"мёртвая\".");
                values.put("how_to_do", "1. Положите штангу на полу перед собой.\n2. Исходное положение: ноги поставьте на ширине плеч, можно чуть уже. Возьмите грифхватом сверху, на ширине немного уже плеч. Поднимите штангу. Слегка согните колени. Спину прогните в пояснице, таз подайте назад.\n3. Во время выполнения упражнения следите, чтобы руки оставались выпрямленными, спина - прогнутой в пояснице, колени не сгибались сильнее, чем было в исходном положении. Движение штанги должно быть максимально ровным и медленным.\n4. На вдохе - опустите штангу вниз, до того положения, в котором спина будет параллельна полу.\n5. На выдохе - возвратитесь в исходное положение, выпрямив туловище.\n6. Выполните нужное количество повторений.");
                values.put("advice", "1. Если берете большой вес, и чувствуете, что запястьям тяжело, используйте лямки.\n2. Чем больше вы сгибаете ноги во время выполнения упражнения, тем сильнее работают ягодицы. Чем меньше - тем сильнее нагружается задняя поверхность бедра. Но самое главное - ноги не должны быть полностью выпрямленными это ведет к чрезмерной нагрузке на подколенные сухожилия.\n3. Использовать гантели более удобно. Но одновременно придется уменьшить и вес. Поэтому, гантели чаще используют девушки.\n4. Следите, чтобы спина была прогнута в пояснице. Если у Вас не получается полностью наклониться не горбя спину, то тогда следует не так низко наклоняться, или чуть сильнее сгибать ноги.\n5. Если чувствуете боли в пояснице, то не рекомендуется вообще делать становую тягу, либо сначала выполните несколько подходов гиперэкстензии. Вообще, перед становой тягой стоит хорошенько размяться и подготовить мышцы и суставы.");
                db.insert("exercise_store", null, values);

//exercise = 76
                values.put("name", "Подъём на икры сидя");
                values.put("target_muscle", "Икры");
                values.put("involved_muscle", "-");
                values.put("level", 1);
                values.put("inventory", "Гантели, Штанга, Другие тренажеры");
                values.put("dumbbells_barbell", 1); //Гантели-Штанга
                values.put("description", "Одно из самых популярных упражнений для икроножной мышцы. Также тренирует камбаловидную мышцу. Выполняется на специальном тренажере. Но если его нет, то можно выполнять на скамье - со штангой, гантелями или используя машину Смитта.");
                values.put("how_to_do", "1. Расположите подставку (брусок или диск от штанги) на расстоянии около 30 см от скамьи.\n2. Исходное положение: сядьте на край скамьи, поставьте носки на подставку (ноги чуть уже ширины плеч). Возьмите штангу обратным хватом (или попросите кого-нибудь подать), положите её на бедра, примерно на 10 см выше коленей.\n3. На выдохе - поднимитесь на носках максимально высоко, задержитесь в верхней точке на 1-2 секунды.\n4. На вдохе - вернитесь в исходное положение.\n5. Сделайте необходимое количество повторений.");
                values.put("advice", "1. Если хотите накачать объёмную голень - то попробуйте сочетать подъёмы на носки как сидя так и стоя, так как они развивают мышцы по разному. Подъём на икры сидя больше нагружает камбаловидную мышцу.\n2. Достаточно большое утяжеление держать на бёдрах неудобно. Советуем попросить товарища (или подругу, кому как нравится) сесть вам на колени..\n3. Использовать подставку под носки обязательно - без них упражнения утратит свою эффективность примерно в два раза.");
                db.insert("exercise_store", null, values);

//exercise = 77
                values.put("name", "Гиперэкстензия");
                values.put("target_muscle", "Нижняя часть спины");
                values.put("involved_muscle", "Ягодицы, Бедра");
                values.put("level", 1);
                values.put("inventory", "Гиперэкстензия");
                values.put("other_trainers", 1); //Другие тренажеры
                values.put("description", "Простое, и полезное упражнение! Прекрасно нагружает мышцы поясницы и ягодиц, а самое главное - при этом никакой нагрузки на позвоночник. Если у вас, грыжа или остеохондроз межпозвоночных дисков - гиперэкстензию на тренажере делать просто необходимо.");
                values.put("how_to_do", "1. Отрегулируйте скамью для гиперэкстензии так, чтобы верхняя часть бедер упиралась в тренажер, и можно свободно выполнить наклон вперед, сгибаясь в талии.\n2. Лягте вниз лицом на скамью. Зафиксируйте голени под подставкой.\n3. Исходное положение: туловище прямо, руки скрещены на груди или за головой.\n4. На вдохе - медленно наклоняйтесь вперед, сгибаясь в талии. Спину держите прямо. Наклон продолжайте до тех пор, пока не почувствуете, что дальше наклоняться вперед без сгибания спины невозможен.\n5. На выдохе - поднимите туловище в исходное положение.\n6. Выполните необходимое количество повторений.");
                values.put("advice", "1. Техника выполнения гиперэкстензии - в первую очередь плавные движения, иначе вы можете травмировать спину.\n2. Можете выполнять упражнение с отягощением (диск от штанги). Держите его перед грудью или за головой.\n3. Гиперэкстензию легко делать и в домашних условиях. Для этого вполне подойдёт кровать или диван. Ещё понадобиться напарник, который будет держать Ваши ноги.\n4. Это упражнение идеально для поясничных мышц спины. Вместе с тем, оно не оказывает нагрузки на позвончник. Поэтому гиперэкстензия поможет укрепить мышцы поясницы всем кто страдает от остеохондроза, грыжи дисков позвоночника и протрузии. Также помогает от болей в пояснице.");
                db.insert("exercise_store", null, values);

                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }
}
