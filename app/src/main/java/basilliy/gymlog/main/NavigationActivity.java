package basilliy.gymlog.main;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import basilliy.gymlog.R;
import basilliy.gymlog.interfaces.OnBackPressedListener;
import basilliy.gymlog.navigation.ListProgramContainer;
import basilliy.gymlog.navigation.NavigationActiveProgramFragment;
import basilliy.gymlog.navigation.NavigationCalendarFragment;
import basilliy.gymlog.navigation.NavigationGraphFragment;
import basilliy.gymlog.newProgram.CreateProgramFragment;
import basilliy.gymlog.pattern.ToolbarActivity;

public class NavigationActivity extends ToolbarActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private OnBackPressedListener backPressedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        initAppBar(R.id.appBar);
        Toolbar toolbar = initToolbar(R.id.toolbar);
        addToolbarFirstLine(R.id.layout_toolbar_main);
        addToolbarFirstLine(R.id.layout_toolbar_list_program);
        addToolbarFirstLine(R.id.layout_toolbar_new_program);

        addToolbarSecondLine(R.id.tabs);
        addToolbarSecondLine(R.id.layout_toolbar_new_program_marker);
        addToolbarSecondLine(R.id.layout_toolbar_new_program_count);

        setFragment(R.id.nav_list_program);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (backPressedListener == null || !backPressedListener.onBackPressed())
                super.onBackPressed();
        }
    }

    public void setBackPressedFragment(OnBackPressedListener fragment) {
        backPressedListener = fragment;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        setFragment(item.getItemId());
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setFragment(int idFragment) {
        Fragment f;
        switch(idFragment) {
            case R.id.nav_active_program:
                f = new NavigationActiveProgramFragment();
                break;
            case R.id.nav_list_program:
                f = new ListProgramContainer();
                break;
            case R.id.nav_new_program:
                f = new CreateProgramFragment();
                break;
            case R.id.nav_calendar:
                f = new NavigationCalendarFragment();
                break;
            case R.id.nav_graph:
                f = new NavigationGraphFragment();
                break;
            default:
                f = new NavigationActiveProgramFragment();

        }
        showToolbarFirstLine(0);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, f).commit();
    }
}
