package basilliy.gymlog.main;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import basilliy.gymlog.R;
import basilliy.gymlog.datamodel.Day;
import basilliy.gymlog.datamodel.Program;

public class D {

    public static Program CREATED_PROGRAM;

    public static void log(String s) {
        if (s == null)
            Log.d("GymLog", "null");
        else
            Log.d("GymLog", s);
    }

    public static void snack(View view, String text) {
        Snackbar snack = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
        TextView txt = (TextView) snack.getView().findViewById(android.support.design.R.id.snackbar_text);
        snack.getView().setBackgroundResource(R.color.colorPrimary);
        txt.setTextColor(Color.WHITE);
        snack.show();
    }

    public static void snack(View view, int res) {
        Snackbar snack = Snackbar.make(view, res, Snackbar.LENGTH_LONG);
        TextView txt = (TextView) snack.getView().findViewById(android.support.design.R.id.snackbar_text);
        snack.getView().setBackgroundResource(R.color.colorPrimary);
        txt.setTextColor(Color.WHITE);
        snack.show();
    }

    public class conts {
        public static final String DAY = "day";
        public static final String EXERCISE = "exercise";
        public static final String APPROACH = "approach";
        public static final String REPS = "reps";
        public static final String WEIGHT = "weight";
    }


    public class name {

    }

    public class fragment {
        public static final int active_program = 1000;
        public static final int list_program = 1001;
        public static final int new_program = 1002;
        public static final int calendar = 1003;
        public static final int graph = 1004;
    }

    public class preferences {
        public static final String pref_name = "gymlog";
        public static final String level = "level";
        public static final String barbell = "barbell"; // Штанга
        public static final String dumbbells = "dumbbells"; // Гантели
        public static final String dumbbells_barbell = "dumbbells_barbell"; // Гантели
        public static final String power_bench = "power_bench"; // Силовая скамья
        public static final String power_frame = "power_frame"; // Силовая рама
        public static final String rope_trainers = "rope_trainers"; // Тросовые тренажеры
        public static final String lever_trainers = "lever_trainers"; // Рычажные тренажеры
        public static final String other_trainers = "other_trainers"; // Другие тренажеры
    }

    public class new_program {
        public class preferences {
            public static final String file = "gymlog_new_program";
            public static final String last_reps = "last_reps";
            public static final String last_weight = "last_weight";
        }
    }

}
