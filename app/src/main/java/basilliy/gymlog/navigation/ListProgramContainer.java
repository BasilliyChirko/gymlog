package basilliy.gymlog.navigation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import basilliy.gymlog.R;
import basilliy.gymlog.fragments.list.program.BasicListProgram;
import basilliy.gymlog.fragments.list.program.MyListProgramFragment;
import basilliy.gymlog.main.NavigationActivity;
import basilliy.gymlog.pattern.TabsPagerAdapter;


public class ListProgramContainer extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.self_view_pager, container, false);

        NavigationActivity activity = (NavigationActivity) getActivity();
        activity.showToolbarFirstLine(R.id.layout_toolbar_list_program);
        TabLayout tabs = (TabLayout) activity.showToolbarSecondLine(R.id.tabs);

        ViewPager pager = (ViewPager) v.findViewById(R.id.pager);

        TabsPagerAdapter adapter = new TabsPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new MyListProgramFragment(), "Мои программы");
        adapter.addFragment(new BasicListProgram(), "Готовые программы");

        pager.setAdapter(adapter);
        tabs.setupWithViewPager(pager);

        return v;
    }
}
