package basilliy.gymlog.interfaces;

import android.support.v4.app.Fragment;

public interface ChildFragment {
    Fragment setParentFragment(Fragment fragment);
}
