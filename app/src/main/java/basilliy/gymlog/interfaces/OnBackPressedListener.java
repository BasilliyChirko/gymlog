package basilliy.gymlog.interfaces;

public interface OnBackPressedListener {
    boolean onBackPressed();
}
