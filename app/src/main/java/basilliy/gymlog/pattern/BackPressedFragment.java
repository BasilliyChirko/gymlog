package basilliy.gymlog.pattern;

import android.app.Activity;
import android.support.v4.app.Fragment;

import basilliy.gymlog.interfaces.OnBackPressedListener;
import basilliy.gymlog.main.NavigationActivity;

public abstract class BackPressedFragment extends Fragment implements OnBackPressedListener {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((NavigationActivity) activity).setBackPressedFragment(this);
    }

    @Override
    public abstract boolean onBackPressed();
}
