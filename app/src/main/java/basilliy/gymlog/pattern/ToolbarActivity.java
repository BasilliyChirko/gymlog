package basilliy.gymlog.pattern;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class ToolbarActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ArrayList<View> toolbarFirstLine = new ArrayList<>();
    private ArrayList<View> toolbarSecondLine = new ArrayList<>();

    private View appBar;

    public void initAppBar(int res) {
        appBar = findViewById(res);
    }

    public Toolbar initToolbar(int res) {
        toolbar = (Toolbar) findViewById(res);
        setSupportActionBar(toolbar);
        return toolbar;
    }

    public void addToolbarFirstLine(int res) {
        toolbarFirstLine.add(toolbar.findViewById(res));
    }

    public void addToolbarSecondLine(int res) {
        toolbarSecondLine.add(findViewById(res));
    }

    public View showToolbarFirstLine(int res) {
        for (View view : toolbarFirstLine)
            view.setVisibility(View.GONE);
        if (res != 0) {
            View view = toolbar.findViewById(res);
            view.setVisibility(View.VISIBLE);
            return view;
        }
        return null;
    }

    public View showToolbarSecondLine(int res) {
        for (View view : toolbarSecondLine)
            view.setVisibility(View.GONE);
        if (res != 0) {
            View view = findViewById(res);
            view.setVisibility(View.VISIBLE);
            if (appBar != null) {
                float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
                ViewGroup.LayoutParams params =  appBar.getLayoutParams();
                params.height = (int) pixels;
                appBar.setLayoutParams(params);
            }
            return view;
        } else if (appBar != null) {
            float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics());
            ViewGroup.LayoutParams params =  appBar.getLayoutParams();
            params.height = (int) pixels;
            appBar.setLayoutParams(params);
        }
        return null;
    }

    public void setDisplayHomeAsUpEnabled(boolean b) {
        if (b)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();  return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
